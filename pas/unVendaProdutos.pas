unit unVendaProdutos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids, DBCtrls, ExtCtrls, AppEvnts,
  DB, DBClient, ShellAPI, frxClass, frxDBSet, IniFiles;

type
  TfrmVendaProdutos = class(TForm)
    bvl1: TBevel;
    edtCliente: TEdit;
    dblCliente: TDBLookupComboBox;
    lb1: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    edtCodProd: TEdit;
    lb5: TLabel;
    edtDescricao: TEdit;
    bvl3: TBevel;
    lb6: TLabel;
    edtQtde: TEdit;
    lb7: TLabel;
    edtVlrUnit: TEdit;
    lb8: TLabel;
    edtVlrTotal: TEdit;
    GridItens: TDBGrid;
    btnInserir: TBitBtn;
    btnApagar: TBitBtn;
    btnCancelar: TBitBtn;
    btnConcluir: TBitBtn;
    btnClear: TBitBtn;
    edtSubTotal: TEdit;
    shp1: TShape;
    lb9: TLabel;
    btnCalculadora: TBitBtn;
    btnBuscarProduto: TBitBtn;
    ApplicationEvents1: TApplicationEvents;
    dsItens: TDataSource;
    cdsItens: TClientDataSet;
    cdsItensCODIGO: TIntegerField;
    cdsItensDESCRICAO: TStringField;
    cdsItensQTDE: TFloatField;
    cdsItensVLRUNIT: TCurrencyField;
    cdsItensVLRTOTAL: TCurrencyField;
    pnConclusaoPedido: TPanel;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    pn1: TPanel;
    btnConfirmar: TBitBtn;
    btnCancel: TBitBtn;
    edtDescPerc: TEdit;
    edtDescReal: TEdit;
    edtVlrTotVenda: TEdit;
    rgTpVenda: TRadioGroup;
    lb2: TLabel;
    edtVlrRecebido: TEdit;
    lb10: TLabel;
    edtVlrTroco: TEdit;
    lb14: TLabel;
    edtVlrBrutoVenda: TEdit;
    frxRelVenda: TfrxReport;
    frxDBVendaCab: TfrxDBDataset;
    frxDBVendaItens: TfrxDBDataset;
    procedure FormCreate(Sender: TObject);
    procedure dblClienteExit(Sender: TObject);
    procedure edtClienteExit(Sender: TObject);
    procedure edtCodProdExit(Sender: TObject);
    procedure edtQtdeExit(Sender: TObject);
    procedure edtVlrUnitExit(Sender: TObject);
    procedure ApplicationEvents1ShortCut(var Msg: TWMKey;
      var Handled: Boolean);
    procedure btnClearClick(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure btnApagarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure editsKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConcluirClick(Sender: TObject);
    procedure btnCalculadoraClick(Sender: TObject);
    procedure btnBuscarProdutoClick(Sender: TObject);
    procedure edtDescontosExit(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure edtVlrRecebidoExit(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FValve: Currency;
    fVendSSaldo, fImprDireta, fContrEst, fCaminhoImpressora : String;
    { Private declarations }
    procedure ClearEditors;
    procedure CalculaVlrTotal;
    procedure AdicionarProduto;
    procedure RemoverProduto;
    procedure LoadValueMemory;
    procedure SetValve(const Value: Currency);
    procedure CancelarPedido;
    procedure ConcluirPedido;
    procedure ConsultarProduto;
    procedure WriteDataTables;
    procedure PrintCupom;
    procedure ImpressaoDireta;
  protected
     property Valve : Currency read FValve write SetValve;
  public
    { Public declarations }
  end;

var
  frmVendaProdutos: TfrmVendaProdutos;

implementation

{$R *.dfm}

uses unDmPedidos, unConsultaProdutos;

{ TfrmVendaProdutos }

procedure TfrmVendaProdutos.ClearEditors;
 begin
  edtCodProd.Text := '';
  edtDescricao.Text := '';
  edtVlrTotal.Text := '0,00';
  edtVlrUnit.Text := '0,00';
  edtQtde.Text := '1,00';
 end;

procedure TfrmVendaProdutos.FormCreate(Sender: TObject);
var
  IniFile : TIniFile;
 begin
  IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini');
  try
   fVendSSaldo := IniFile.ReadString('ESTOQUE', 'Vende Sem Saldo', '');
   fContrEst := IniFile.ReadString('ESTOQUE', 'Controla Estoque', '');
   fImprDireta := IniFile.ReadString('IMPRESSAO', 'Impressao Direta', '');
   fCaminhoImpressora := IniFile.ReadString('IMPRESSAO', 'Local Impressora', '');
  finally
   IniFile.Free;
  end;
  with dmPedidos do
   try
    if (qryCliente.Active = False) then
     qryCliente.Open;
    if (qryProduto.Active = False) then
     qryProduto.Open;
   except
    on e : Exception do
     ShowMessage('Erro ao tentar abrir as tabelas neces�rias � venda!' + e.Message);
   end;
 end;

procedure TfrmVendaProdutos.dblClienteExit(Sender: TObject);
 begin
  if dblCliente.KeyValue <> null then
   edtCliente.Text := dblCliente.KeyValue;
 end;

procedure TfrmVendaProdutos.edtClienteExit(Sender: TObject);
 begin
  with dmPedidos do
   begin
    if (edtCliente.Text <> '') then
     if(qryCliente.Locate('ID', edtCliente.Text,[])) then
      dblCliente.KeyValue := qryClienteID.AsInteger
     else
      begin
       Application.MessageBox('Cliente n�o encontrado a partir de c�digo digitado!','Erro na Pesquisa',MB_OK + MB_ICONWARNING);
       edtCliente.SetFocus;
      end;
   end;
 end;

procedure TfrmVendaProdutos.edtCodProdExit(Sender: TObject);
 begin
  with dmPedidos do
   begin
    if (edtCodProd.Text <> '') then
     if(qryProduto.Locate('ID', edtCodProd.Text,[])) then
      begin             
       edtDescricao.Text := qryProdutoDECRICAO.AsString;
       edtVlrUnit.Text   := qryProdutoVLR_VENDA.AsString;
       CalculaVlrTotal;
      end
     else
      begin
       Application.MessageBox('Produto n�o encontrado a partir de c�digo digitado!','Erro na Pesquisa',MB_OK + MB_ICONWARNING);
       edtCodProd.SetFocus;
      end;
   end;
 end;

procedure TfrmVendaProdutos.CalculaVlrTotal;
 begin
  edtVlrTotal.Text := FormatCurr(',0.00', StrToFloat(edtQtde.Text) * StrToFloat(edtVlrUnit.Text));
 end;

procedure TfrmVendaProdutos.edtQtdeExit(Sender: TObject);
 begin
  CalculaVlrTotal;
 end;

procedure TfrmVendaProdutos.edtVlrUnitExit(Sender: TObject);
 begin
  CalculaVlrTotal;
 end;

procedure TfrmVendaProdutos.ApplicationEvents1ShortCut(var Msg: TWMKey;
  var Handled: Boolean);
var
  Shift : TShiftState;
 begin
  Shift := KeyboardStateToShiftState;
  case Msg.CharCode of
   VK_F1 :
      AdicionarProduto;
   VK_F2 :
      RemoverProduto;
   VK_F3 :
      CancelarPedido;
   VK_F4 :
      ConcluirPedido;
   VK_F7 :
      ConsultarProduto;
   VK_F8 :
      btnCalculadora.Click;
   VK_ESCAPE :
      ClearEditors;
  end;
 end;

procedure TfrmVendaProdutos.btnClearClick(Sender: TObject);
 begin
  ClearEditors;
 end;

procedure TfrmVendaProdutos.AdicionarProduto;
 begin   
  if (edtCodProd.Text = '') or (edtDescricao.Text = '') then
   begin
    Application.MessageBox('Informe o Produto antes de continuar!','Erro na Inser��o',MB_OK + MB_ICONWARNING);
    edtCodProd.SetFocus;
    Abort;
   end;

  if (fVendSSaldo <> 'S') then
   begin
    if dmPedidos.qryProdutoSALDO.AsCurrency < StrToCurr(edtQtde.Text) then
     begin
      Application.MessageBox('A Quantidade informada para a venda � maior que o saldo dispon�vel.' +
                             'Informe uma quantidade menor, ou altere o produto!', 'Saldo Insuficiente',
                             MB_OK + MB_ICONWARNING);
      edtQtde.SetFocus;
      Abort;
     end;
   end;

  with cdsItens do
   begin
    First;
    while not(Eof) do
     begin
      if (cdsItensCODIGO.AsString = edtCodProd.Text) then
       begin
        Application.MessageBox('Aten��o! Produto j� lan�ado nesta venda!', 'Produto j� lan�ado', MB_OK + MB_ICONWARNING);
        Exit;
       end;
      Next;
     end;
   end;
  
  CalculaVlrTotal;
  if (edtVlrUnit.Text = '0') or (edtVlrUnit.Text = '0,00') then
   begin
    Application.MessageBox('Aten��o valor unit�rio menor ou igual a zero!','Erro na Inser��o',MB_OK + MB_ICONWARNING);
    Abort;
   end;
  if (edtVlrTotal.Text = '0') or (edtVlrTotal.Text = '0,00') then
   begin
    Application.MessageBox('Aten��o valor total menor ou igual a zero!','Erro na Inser��o',MB_OK + MB_ICONWARNING);
    Abort;
   end;
  LoadValueMemory;
  Valve := Valve + cdsItensVLRTOTAL.AsCurrency;
  edtSubTotal.Text := FormatCurr(',0.00', Valve);
  ClearEditors;
  edtCodProd.SetFocus;
 end;

procedure TfrmVendaProdutos.RemoverProduto;
 begin
  if cdsItens.IsEmpty then
   Application.MessageBox('Venda sem produtos para serem excluidos!', 'Tabela Vazia',MB_OK + MB_ICONEXCLAMATION)
  else
   begin
    if not(GridItens.Focused) then
     Application.MessageBox('Selecione o produto que deseja excluir!', 'Sele��o de Produto',MB_OK + MB_ICONEXCLAMATION)
    else
    if (Application.MessageBox('Deseja realmente excluir o produto selecionado?','Apagar Produto',
        MB_YESNO+MB_ICONQUESTION) = idYES) then
     begin
      Valve := Valve - cdsItensVLRTOTAL.AsCurrency;
      edtSubTotal.Text := FormatCurr(',0.00', Valve);;
      cdsItens.Delete;
     end;
   end;
 end;

procedure TfrmVendaProdutos.LoadValueMemory;
 begin
  with cdsItens do
   begin
    Append;
    FieldByName('CODIGO').AsInteger   := StrToInt(edtCodProd.Text);
    FieldByName('DESCRICAO').AsString := edtDescricao.Text;
    FieldByName('QTDE').AsFloat       := StrToFloat(edtQtde.Text);
    FieldByName('VLRUNIT').AsCurrency := StrToCurr(edtVlrUnit.Text);
    FieldByName('VLRTOTAL').AsCurrency:= StrToCurr(edtVlrTotal.Text);
    Post;
   end;
 end;

procedure TfrmVendaProdutos.SetValve(const Value: Currency);
 begin
  FValve := Value;
 end;

procedure TfrmVendaProdutos.btnInserirClick(Sender: TObject);
 begin
  AdicionarProduto;
 end;

procedure TfrmVendaProdutos.btnApagarClick(Sender: TObject);
 begin
  RemoverProduto;
 end;

procedure TfrmVendaProdutos.FormKeyPress(Sender: TObject; var Key: Char);
 begin
  if (Key = #13) then
   begin
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);
   end;
 end;

procedure TfrmVendaProdutos.editsKeyPress(Sender: TObject; var Key: Char);
 begin
  if not(Key in['0'..'9',',',#8]) then
   Key := #0;
 end;

procedure TfrmVendaProdutos.CancelarPedido;
 begin
  if (Application.MessageBox('Deseja realmente cancelar este pedido?','Cancelar Pedido',
                            MB_YESNO+MB_ICONQUESTION) = idYES) then
   begin
    while not(cdsItens.Eof) do
     cdsItens.Delete;
    Application.MessageBox('Pedido Cancelado com Sucesso!', 'Sucesso', MB_OK+MB_ICONINFORMATION);
    Close;
   end;
 end;

procedure TfrmVendaProdutos.btnCancelarClick(Sender: TObject);
 begin
  CancelarPedido;
 end;

procedure TfrmVendaProdutos.ConcluirPedido;
 begin
  if (dblCliente.KeyValue = null) then
   begin
    Application.MessageBox('Informe o cliente antes de tentar finalizar a Venda!', 'Venda sem Cliente', MB_OK+MB_ICONINFORMATION);
    edtCliente.SetFocus;
    Abort;
   end;
  if not(cdsItens.IsEmpty) then
   begin
    edtVlrTotVenda.Text := edtSubTotal.Text;
    edtVlrRecebido.Text := edtSubTotal.Text;
    edtVlrBrutoVenda.Text := edtSubTotal.Text;
    pnConclusaoPedido.Visible := True;
    rgTpVenda.SetFocus;
   end
  else
   Application.MessageBox('Venda n�o iniciada insira pelo menos um item antes de tentar finalizar!', 'Venda sem Produtos', MB_OK+MB_ICONINFORMATION);
 end;

procedure TfrmVendaProdutos.btnConcluirClick(Sender: TObject);
 begin
  ConcluirPedido;
 end;

procedure TfrmVendaProdutos.btnCalculadoraClick(Sender: TObject);
var
  CalcInst : HWND;
 begin
  CalcInst := FindWindow(nIl, 'Calculadora');
  if CalcInst > 0 then
    SetForegroundWindow(CalcInst)
  else
  begin
    ShellExecute(0, nil, PChar('CALC.EXE'), nil, nil, SW_ShowNORMAL);
    SetForegroundWindow(CalcInst);
  end;
 
 end;
procedure TfrmVendaProdutos.ConsultarProduto;
var
  CodProd : String;
 begin
  CodProd := '';
  if ShowConsultaProduto(CodProd) then
   begin
    edtCodProd.Text := CodProd;
    edtCodProdExit(nil);
   end;
 end;

procedure TfrmVendaProdutos.btnBuscarProdutoClick(Sender: TObject);
 begin
  ConsultarProduto;
 end;

procedure TfrmVendaProdutos.edtDescontosExit(Sender: TObject);
var
  DescReal, DescPer, VlrRecebido : Double;
 begin
  DescPer := 0;
  DescReal:= 0;
  VlrRecebido:= 0;
  if (edtDescReal.Text <> '') then
   DescReal := StrToFloat(edtDescReal.Text);
  if (edtDescPerc.Text <> '') then
   DescPer  := StrToFloat(edtDescPerc.Text);
  if (edtVlrBrutoVenda.Text <> '') then
   VlrRecebido:= StrToFloat(edtVlrBrutoVenda.Text);

  //Valida��o dos valores dos descontos % e R$ -- J�nior Lira 08/09/2018
  if (DescReal > VlrRecebido) then
   begin
    Application.MessageBox('O Desconto informado � maior que o valor da venda!',
                           'Valor errado', MB_OK + MB_ICONWARNING);
    edtDescReal.SetFocus;
    Abort;
   end;
  if (DescPer < 0) or (DescPer > 100) then
   begin
    Application.MessageBox('O Desconto percentual informado est� invalido. Informe um valor entre 0 e 100!',
                           'Valor errado', MB_OK + MB_ICONWARNING);
    edtDescPerc.SetFocus;
    Abort;
   end;

  if (Sender = edtDescPerc) then
   DescReal := (DescPer * VlrRecebido) / 100
  else
   DescPer := (DescReal * 100) / VlrRecebido;
  VlrRecebido := VlrRecebido - DescReal;
  edtDescPerc.Text := FormatFloat(',0.00',DescPer);
  edtDescReal.Text := FormatFloat(',0.00',DescReal);
  edtVlrRecebido.Text := FormatFloat(',0.00',VlrRecebido);
  edtVlrTotVenda.Text := edtVlrRecebido.Text;
 end;

procedure TfrmVendaProdutos.btnCancelClick(Sender: TObject);
 begin
  pnConclusaoPedido.Visible := False;
 end;

procedure TfrmVendaProdutos.edtVlrRecebidoExit(Sender: TObject);
var
  vlrTot, vlrReceb, troco : Double;
 begin
  vlrTot := StrToFloat(edtVlrTotVenda.Text);
  if (edtVlrRecebido.Text = '') then
   vlrReceb := 0
  else
   vlrReceb:= StrToFloat(edtVlrRecebido.Text);
  if vlrReceb < vlrTot then
   begin
    Application.MessageBox('Valor menor que o esperado, caso exista descontos, informe o mesmo no ' +
                           'campo correpondente!', 'Valor errado', MB_OK + MB_ICONWARNING);
    edtVlrRecebido.SetFocus;
    Abort;
   end;
  troco := vlrReceb - vlrTot;
  edtVlrTroco.Text := FloatToStr(troco);
 end;

procedure TfrmVendaProdutos.btnConfirmarClick(Sender: TObject);
 begin
  if (StrToCurr(edtVlrRecebido.Text) <= 0) then
   begin
    if(Application.MessageBox('Aten��o! O valor total recebido est� zerado. Continua mesmo assim?',
                              'Valor Total Zerado',MB_YESNO + MB_ICONQUESTION) <> idYes) then
     begin
      edtVlrRecebido.SetFocus;
      Abort;
     end;
   end;

  //Perssitindo os Dados da Venda no Banco de Dados -- J�nior Lira 08/09/2018
  WriteDataTables;

  if(Application.MessageBox('Venda Concluida com Sucesso. Deseja imprimir o cupom da mesma?',
                              'Fechamento do Pedido', MB_YESNO + MB_ICONQUESTION) = idYes) then
   PrintCupom
  else
   begin
    while not(cdsItens.IsEmpty) do
     cdsItens.Delete;
    Close;
   end;


 end;

procedure TfrmVendaProdutos.WriteDataTables;
 begin
  with dmPedidos do
   begin
    //Cabe�alho da Venda
    try
     if (qryVendaCab.Active = False) then
      qryVendaCab.Open;
     qryVendaCab.Insert;
     qryVendaCabID_CLIENTE.AsInteger := dblCliente.KeyValue;
     if (rgTpVenda.ItemIndex = 0) then
      qryVendaCabTP_VENDA.AsString := 'V'
     else
      qryVendaCabTP_VENDA.AsString := 'P';
     qryVendaCabVLR_TOT_VENDA.AsCurrency := StrToCurr(edtVlrBrutoVenda.Text);
     qryVendaCabVLR_LIQ_VENDA.AsCurrency := StrToCurr(edtVlrTotVenda.Text);
     qryVendaCabVLR_DESC_REAL.AsCurrency := StrToCurr(edtDescReal.Text);
     qryVendaCabVLR_DESC_PERC.AsCurrency := StrToCurr(edtDescPerc.Text);
     qryVendaCabDT_VENDA.AsDateTime := Date;
     qryVendaCabHR_VENDA.AsDateTime := Time;
     qryVendaCab.Post;
     qryVendaCab.ApplyUpdates;
    except
     on E : Exception do
     begin
      ShowMessage('Erro ao tentar Inserir dados no Cabecalho da Venda! ' + E.Message);
      qryVendaCab.Cancel;
      qryVendaCab.CancelUpdates;
      Abort;
     end;
    end;

    //Itens da Venda
    try
     if (qryVendaItens.Active = False) then
      qryVendaItens.Open;

     cdsItens.First;
     while not(cdsItens.Eof) do
      begin
       qryVendaItens.Insert;
       qryVendaItensID_VENDA.AsInteger     := qryVendaCabID.AsInteger;
       qryVendaItensID_PRODUTO.AsInteger   := cdsItensCODIGO.AsInteger;
       qryVendaItensQTDE.AsCurrency        := cdsItensQTDE.AsCurrency;
       qryVendaItensVLR_ITEM.AsCurrency    := cdsItensVLRUNIT.AsCurrency;
       qryVendaItensVLT_TOT_ITEM.AsCurrency:= cdsItensVLRTOTAL.AsCurrency;
       qryVendaItens.Post;
       qryVendaItens.ApplyUpdates;

       if (fContrEst = 'S') then
        begin
         try
          if (qryProduto.Active = False) then
            qryProduto.Open;
          qryProduto.Locate('ID',cdsItensCODIGO.AsString,[]);
          qryProduto.Edit;
          qryProdutoSALDO.AsCurrency := qryProdutoSALDO.AsCurrency - cdsItensQTDE.AsCurrency;
          qryProduto.Post;
          qryProduto.ApplyUpdates;
         except
          on E : Exception do
           begin
            ShowMessage('Erro ao tentar alterar o Saldo dos Produtos! ' + E.Message);
            qryProduto.Cancel;
            qryProduto.CancelUpdates;
            Abort;
           end;
         end;
        end;
       cdsItens.Next;
      end;
    except
     on E : Exception do
     begin
      ShowMessage('Erro ao tentar Inserir os itens da Venda! ' + E.Message);
      qryVendaItens.Cancel;
      qryVendaItens.CancelUpdates;
      Abort;
     end;
    end;
   end;
 end;

procedure TfrmVendaProdutos.PrintCupom;

 begin
  try
   if (fImprDireta = 'S') then
    begin
     ImpressaoDireta;
    end
   else
    begin
     frxRelVenda.Variables['CLIENTE'] := QuotedStr(dblCliente.Text);
     frxRelVenda.PrepareReport(True);
     frxRelVenda.ShowPreparedReport;
    end;
  finally
   Application.MessageBox('Impress�o Concluida!','Aviso',MB_OK+MB_ICONINFORMATION);
   while not(cdsItens.IsEmpty) do
    cdsItens.Delete;
   Close;
  end;
 end;

procedure TfrmVendaProdutos.ImpressaoDireta;
var
  Print : TextFile;
 begin
  try
   Screen.Cursor := crHourGlass;

   AssignFile(Print, fCaminhoImpressora);
   Rewrite(Print);

   with dmPedidos do
    begin
     Writeln(print,'========================================');
     Writeln(print,'      Cupom de Venda de Produtos        ');
     Writeln(print,'========================================');
     Writeln(print,#27#72,' Pedido N�: ' + qryVendaCabID.AsString + ' DT-HR: ' +
             qryVendaCabDT_VENDA.AsString + ' - ' + qryVendaCabHR_VENDA.AsString);
     Writeln(print,#27#72,'Cliente: ' + qryVendaCabID_CLIENTE.AsString + ' - ' + dblCliente.Text);
     Writeln(print,'========================================');
     Writeln(print,' DESCRICAO                              ');
     Writeln(print,'QTDE.      UNIT.        TOTAL           ');
     Writeln(print,'----------------------------------------');
     cdsItens.First;
     while not(cdsItens.EOF) do
      begin
       Writeln(print,cdsItensDESCRICAO.AsString);
       Writeln(print,'  ' + FormatFloat(',0.00',cdsItensQTDE.AsFloat) + '    *    ' +
               FormatFloat(',0.00',cdsItensVLRUNIT.AsFloat) + '            ' + FormatFloat(',0.00',cdsItensVLRTOTAL.AsFloat));
       cdsItens.Next;
      end;
     Writeln(print,'----------------------------------------');
     Writeln(print,'       VLR. BRUTO DA VENDA ->  ' + FormatFloat(',0.00',StrToCurr(edtVlrBrutoVenda.Text)));
     Writeln(print,'       VLR. DESCONTOS      ->  ' + FormatFloat(',0.00',StrToCurr(edtDescReal.Text)));
     Writeln(print,'       VLR. LIQUIDO DA VENDA  ->  ' + FormatFloat(',0.00',StrToCurr(edtVlrTotVenda.Text)));
     Writeln(print,'========================================');  
    end;
   CloseFile(Print);
  finally
   Screen.Cursor := crDefault;
  end;
 end;

procedure TfrmVendaProdutos.FormClose(Sender: TObject;
  var Action: TCloseAction);
 begin
  dmPedidos.qryCliente.Close;
  dmPedidos.qryProduto.Close;
 end;

end.



