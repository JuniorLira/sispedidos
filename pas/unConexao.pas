unit unConexao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ExtCtrls;

type
  TfrmConexao = class(TForm)
    lb1: TLabel;
    bvl1: TBevel;
    btnLocal: TButton;
    edtFile: TEdit;
    btnConfirmar: TBitBtn;
    dlgBase: TOpenDialog;
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnLocalClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function ShowConexao(var FileConnection : String) : boolean;

var
  frmConexao: TfrmConexao;

implementation

{$R *.dfm}

function ShowConexao(var FileConnection : String) : boolean;
 begin
  with TfrmConexao.Create(Application) do
   try
    Result := ShowModal = mrYes;
   finally
    if Result then
     FileConnection := edtFile.Text;
    Free;
   end;
 end;

procedure TfrmConexao.btnConfirmarClick(Sender: TObject);
 begin
  if (edtFile.Text = '') then
   begin
    Application.MessageBox('Informe o caminho do banco de dados antes de continuar!',
                           'Aten��o!',MB_OK + MB_ICONEXCLAMATION);
    edtFile.SetFocus;
    Abort;
   end;
  ModalResult := mrYes;
 end;

procedure TfrmConexao.btnLocalClick(Sender: TObject);
 begin
  dlgBase.Execute();
  edtFile.Text := dlgBase.FileName;
 end;

end.
