object dmPedidos: TdmPedidos
  OldCreateOrder = False
  Left = 502
  Top = 301
  Height = 214
  Width = 486
  object Banco: TIBDatabase
    Params.Strings = (
      'user_name=SYSDBA'
      'password=masterkey')
    LoginPrompt = False
    DefaultTransaction = TBanco
    IdleTimer = 0
    SQLDialect = 3
    TraceFlags = []
    Left = 16
    Top = 8
  end
  object TBanco: TIBTransaction
    Active = False
    DefaultDatabase = Banco
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    AutoStopAction = saNone
    Left = 55
    Top = 9
  end
  object dsCliente: TDataSource
    DataSet = qryCliente
    Left = 169
    Top = 8
  end
  object dsProduto: TDataSource
    DataSet = qryProduto
    Left = 285
    Top = 8
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 383
    Top = 8
  end
  object qryCliente: TIBDataSet
    Database = Banco
    Transaction = TBanco
    BufferChunks = 1000
    CachedUpdates = False
    DeleteSQL.Strings = (
      'delete from CLIENTE'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into CLIENTE'
      '  (ID, TP_PESSOA, RZ_SOCIAL, NM_FANTASIA, DOCUMENTO, ENDERECO, '
      'NUMERO, '
      
        '   BAIRRO, CEP, CIDADE, UF, TELEFONE, CELULAR, EMAIL, DT_CADASTR' +
        'O, '
      'DT_ALTERACAO)'
      'values'
      
        '  (:ID, :TP_PESSOA, :RZ_SOCIAL, :NM_FANTASIA, :DOCUMENTO, :ENDER' +
        'ECO, '
      ':NUMERO, '
      '   :BAIRRO, :CEP, :CIDADE, :UF, :TELEFONE, :CELULAR, :EMAIL, '
      ':DT_CADASTRO, '
      '   :DT_ALTERACAO)')
    RefreshSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'Select * From CLIENTE')
    ModifySQL.Strings = (
      'update CLIENTE'
      'set'
      '  ID = :ID,'
      '  TP_PESSOA = :TP_PESSOA,'
      '  RZ_SOCIAL = :RZ_SOCIAL,'
      '  NM_FANTASIA = :NM_FANTASIA,'
      '  DOCUMENTO = :DOCUMENTO,'
      '  ENDERECO = :ENDERECO,'
      '  NUMERO = :NUMERO,'
      '  BAIRRO = :BAIRRO,'
      '  CEP = :CEP,'
      '  CIDADE = :CIDADE,'
      '  UF = :UF,'
      '  TELEFONE = :TELEFONE,'
      '  CELULAR = :CELULAR,'
      '  EMAIL = :EMAIL,'
      '  DT_CADASTRO = :DT_CADASTRO,'
      '  DT_ALTERACAO = :DT_ALTERACAO'
      'where'
      '  ID = :OLD_ID')
    GeneratorField.Field = 'ID'
    GeneratorField.Generator = 'GEN_CLIENTE'
    Left = 120
    Top = 8
    object qryClienteID: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'CLIENTE.ID'
      Required = True
    end
    object qryClienteTP_PESSOA: TIBStringField
      DisplayLabel = 'Tp. Pessoa'
      FieldName = 'TP_PESSOA'
      Origin = 'CLIENTE.TP_PESSOA'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qryClienteRZ_SOCIAL: TIBStringField
      DisplayLabel = 'Raz'#227'o social'
      FieldName = 'RZ_SOCIAL'
      Origin = 'CLIENTE.RZ_SOCIAL'
      Size = 150
    end
    object qryClienteNM_FANTASIA: TIBStringField
      DisplayLabel = 'Nome'
      FieldName = 'NM_FANTASIA'
      Origin = 'CLIENTE.NM_FANTASIA'
      Size = 150
    end
    object qryClienteDOCUMENTO: TIBStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      Origin = 'CLIENTE.DOCUMENTO'
      Required = True
      Size = 14
    end
    object qryClienteENDERECO: TIBStringField
      DisplayLabel = 'Endere'#231'o'
      FieldName = 'ENDERECO'
      Origin = 'CLIENTE.ENDERECO'
      Size = 100
    end
    object qryClienteNUMERO: TIBStringField
      DisplayLabel = 'N'#250'mero'
      FieldName = 'NUMERO'
      Origin = 'CLIENTE.NUMERO'
      Size = 10
    end
    object qryClienteBAIRRO: TIBStringField
      DisplayLabel = 'Bairro'
      FieldName = 'BAIRRO'
      Origin = 'CLIENTE.BAIRRO'
      Size = 50
    end
    object qryClienteCEP: TIBStringField
      FieldName = 'CEP'
      Origin = 'CLIENTE.CEP'
      Size = 8
    end
    object qryClienteCIDADE: TIBStringField
      DisplayLabel = 'Cidade'
      FieldName = 'CIDADE'
      Origin = 'CLIENTE.CIDADE'
      Size = 50
    end
    object qryClienteUF: TIBStringField
      FieldName = 'UF'
      Origin = 'CLIENTE.UF'
      FixedChar = True
      Size = 2
    end
    object qryClienteTELEFONE: TIBStringField
      DisplayLabel = 'Telefone'
      FieldName = 'TELEFONE'
      Origin = 'CLIENTE.TELEFONE'
      Size = 15
    end
    object qryClienteCELULAR: TIBStringField
      DisplayLabel = 'Celular'
      FieldName = 'CELULAR'
      Origin = 'CLIENTE.CELULAR'
      Size = 15
    end
    object qryClienteEMAIL: TIBStringField
      DisplayLabel = 'Email'
      FieldName = 'EMAIL'
      Origin = 'CLIENTE.EMAIL'
      Size = 75
    end
    object qryClienteDT_CADASTRO: TDateField
      DisplayLabel = 'Dt. Cadastro'
      FieldName = 'DT_CADASTRO'
      Origin = 'CLIENTE.DT_CADASTRO'
    end
    object qryClienteDT_ALTERACAO: TDateField
      DisplayLabel = 'Dt. Altera'#231#227'o'
      FieldName = 'DT_ALTERACAO'
      Origin = 'CLIENTE.DT_ALTERACAO'
    end
  end
  object qryProduto: TIBDataSet
    Database = Banco
    Transaction = TBanco
    BufferChunks = 1000
    CachedUpdates = False
    DeleteSQL.Strings = (
      'delete from PRODUTO'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into PRODUTO'
      '  (ID, DECRICAO, COD_BARRA, UNIDADE, ID_MARCA, VLR_COMPRA, '
      'VLR_VENDA, QTD_MINIMA, '
      '   SALDO, DT_CADASTRO, DT_ALTERACAO)'
      'values'
      
        '  (:ID, :DECRICAO, :COD_BARRA, :UNIDADE, :ID_MARCA, :VLR_COMPRA,' +
        ' '
      ':VLR_VENDA, '
      '   :QTD_MINIMA, :SALDO, :DT_CADASTRO, :DT_ALTERACAO)')
    RefreshSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'Select * From PRODUTO')
    ModifySQL.Strings = (
      'update PRODUTO'
      'set'
      '  ID = :ID,'
      '  DECRICAO = :DECRICAO,'
      '  COD_BARRA = :COD_BARRA,'
      '  UNIDADE = :UNIDADE,'
      '  ID_MARCA = :ID_MARCA,'
      '  VLR_COMPRA = :VLR_COMPRA,'
      '  VLR_VENDA = :VLR_VENDA,'
      '  QTD_MINIMA = :QTD_MINIMA,'
      '  SALDO = :SALDO,'
      '  DT_CADASTRO = :DT_CADASTRO,'
      '  DT_ALTERACAO = :DT_ALTERACAO'
      'where'
      '  ID = :OLD_ID')
    GeneratorField.Field = 'ID'
    GeneratorField.Generator = 'GEN_PRODUTO'
    Left = 240
    Top = 8
    object qryProdutoID: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'PRODUTO.ID'
      Required = True
    end
    object qryProdutoDECRICAO: TIBStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DECRICAO'
      Origin = 'PRODUTO.DECRICAO'
      Size = 150
    end
    object qryProdutoCOD_BARRA: TIBStringField
      DisplayLabel = 'Cod. Barra'
      FieldName = 'COD_BARRA'
      Origin = 'PRODUTO.COD_BARRA'
      Size = 13
    end
    object qryProdutoUNIDADE: TIBStringField
      DisplayLabel = 'Unidade'
      FieldName = 'UNIDADE'
      Origin = 'PRODUTO.UNIDADE'
    end
    object qryProdutoID_MARCA: TIntegerField
      DisplayLabel = 'Marca'
      FieldName = 'ID_MARCA'
      Origin = 'PRODUTO.ID_MARCA'
      Required = True
    end
    object qryProdutoVLR_COMPRA: TIBBCDField
      DisplayLabel = 'Vlr. Compra'
      FieldName = 'VLR_COMPRA'
      Origin = 'PRODUTO.VLR_COMPRA'
      DisplayFormat = ',0.00'
      EditFormat = ',0.00'
      Precision = 9
      Size = 2
    end
    object qryProdutoVLR_VENDA: TIBBCDField
      DisplayLabel = 'Vlr. Venda'
      FieldName = 'VLR_VENDA'
      Origin = 'PRODUTO.VLR_VENDA'
      DisplayFormat = ',0.00'
      EditFormat = ',0.00'
      Precision = 9
      Size = 2
    end
    object qryProdutoQTD_MINIMA: TIBBCDField
      DisplayLabel = 'Qtd. M'#237'nima'
      FieldName = 'QTD_MINIMA'
      Origin = 'PRODUTO.QTD_MINIMA'
      DisplayFormat = ',0.00'
      EditFormat = ',0.00'
      Precision = 9
      Size = 2
    end
    object qryProdutoSALDO: TIBBCDField
      DisplayLabel = 'Saldo'
      FieldName = 'SALDO'
      Origin = 'PRODUTO.SALDO'
      DisplayFormat = ',0.00'
      EditFormat = ',0.00'
      Precision = 9
      Size = 2
    end
    object qryProdutoDT_CADASTRO: TDateField
      DisplayLabel = 'Dt. Cadastro'
      FieldName = 'DT_CADASTRO'
      Origin = 'PRODUTO.DT_CADASTRO'
    end
    object qryProdutoDT_ALTERACAO: TDateField
      DisplayLabel = 'Dt. Altera'#231#227'o'
      FieldName = 'DT_ALTERACAO'
      Origin = 'PRODUTO.DT_ALTERACAO'
    end
  end
  object qryMarca: TIBDataSet
    Database = Banco
    Transaction = TBanco
    BufferChunks = 1000
    CachedUpdates = False
    DeleteSQL.Strings = (
      'delete from MARCA'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into MARCA'
      '  (ID, NOME, DT_CADASTRO, DT_ALTERACAO)'
      'values'
      '  (:ID, :NOME, :DT_CADASTRO, :DT_ALTERACAO)')
    RefreshSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'Select * From MARCA')
    ModifySQL.Strings = (
      'update MARCA'
      'set'
      '  ID = :ID,'
      '  NOME = :NOME,'
      '  DT_CADASTRO = :DT_CADASTRO,'
      '  DT_ALTERACAO = :DT_ALTERACAO'
      'where'
      '  ID = :OLD_ID')
    GeneratorField.Field = 'ID'
    GeneratorField.Generator = 'GEN_MARCA'
    Left = 344
    Top = 8
    object qryMarcaID: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      Origin = 'MARCA.ID'
      Required = True
    end
    object qryMarcaNOME: TIBStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'MARCA.NOME'
      Size = 150
    end
    object qryMarcaDT_CADASTRO: TDateField
      DisplayLabel = 'Dt. Cadastro'
      FieldName = 'DT_CADASTRO'
      Origin = 'MARCA.DT_CADASTRO'
    end
    object qryMarcaDT_ALTERACAO: TDateField
      DisplayLabel = 'Dt. Altera'#231#227'o'
      FieldName = 'DT_ALTERACAO'
      Origin = 'MARCA.DT_ALTERACAO'
    end
  end
  object qryVendaCab: TIBDataSet
    Database = Banco
    Transaction = TBanco
    BufferChunks = 1000
    CachedUpdates = False
    DeleteSQL.Strings = (
      'delete from VENDA_CAB'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into VENDA_CAB'
      '  (ID, ID_CLIENTE, TP_VENDA, VLR_TOT_VENDA, VLR_LIQ_VENDA, '
      'VLR_DESC_REAL, '
      '   VLR_DESC_PERC, DT_VENDA, HR_VENDA)'
      'values'
      '  (:ID, :ID_CLIENTE, :TP_VENDA, :VLR_TOT_VENDA, :VLR_LIQ_VENDA, '
      ':VLR_DESC_REAL, '
      '   :VLR_DESC_PERC, :DT_VENDA, :HR_VENDA)')
    RefreshSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'Select * From VENDA_CAB')
    ModifySQL.Strings = (
      'update VENDA_CAB'
      'set'
      '  ID = :ID,'
      '  ID_CLIENTE = :ID_CLIENTE,'
      '  TP_VENDA = :TP_VENDA,'
      '  VLR_TOT_VENDA = :VLR_TOT_VENDA,'
      '  VLR_LIQ_VENDA = :VLR_LIQ_VENDA,'
      '  VLR_DESC_REAL = :VLR_DESC_REAL,'
      '  VLR_DESC_PERC = :VLR_DESC_PERC,'
      '  DT_VENDA = :DT_VENDA,'
      '  HR_VENDA = :HR_VENDA'
      'where'
      '  ID = :OLD_ID')
    GeneratorField.Field = 'ID'
    GeneratorField.Generator = 'GEN_VENDA_CAB'
    Left = 120
    Top = 72
    object qryVendaCabID: TIntegerField
      FieldName = 'ID'
      Origin = 'VENDA_CAB.ID'
      Required = True
    end
    object qryVendaCabID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'VENDA_CAB.ID_CLIENTE'
      Required = True
    end
    object qryVendaCabTP_VENDA: TIBStringField
      FieldName = 'TP_VENDA'
      Origin = 'VENDA_CAB.TP_VENDA'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qryVendaCabVLR_TOT_VENDA: TIBBCDField
      FieldName = 'VLR_TOT_VENDA'
      Origin = 'VENDA_CAB.VLR_TOT_VENDA'
      Precision = 9
      Size = 2
    end
    object qryVendaCabVLR_LIQ_VENDA: TIBBCDField
      FieldName = 'VLR_LIQ_VENDA'
      Origin = 'VENDA_CAB.VLR_LIQ_VENDA'
      Precision = 9
      Size = 2
    end
    object qryVendaCabVLR_DESC_REAL: TIBBCDField
      FieldName = 'VLR_DESC_REAL'
      Origin = 'VENDA_CAB.VLR_DESC_REAL'
      Precision = 9
      Size = 2
    end
    object qryVendaCabVLR_DESC_PERC: TIBBCDField
      FieldName = 'VLR_DESC_PERC'
      Origin = 'VENDA_CAB.VLR_DESC_PERC'
      Precision = 9
      Size = 2
    end
    object qryVendaCabDT_VENDA: TDateField
      FieldName = 'DT_VENDA'
      Origin = 'VENDA_CAB.DT_VENDA'
    end
    object qryVendaCabHR_VENDA: TTimeField
      FieldName = 'HR_VENDA'
      Origin = 'VENDA_CAB.HR_VENDA'
    end
  end
  object qryVendaItens: TIBDataSet
    Database = Banco
    Transaction = TBanco
    BufferChunks = 1000
    CachedUpdates = False
    DeleteSQL.Strings = (
      'delete from VENDA_ITENS'
      'where'
      '  ID_VENDA = :OLD_ID_VENDA and'
      '  ID_PRODUTO = :OLD_ID_PRODUTO')
    InsertSQL.Strings = (
      'insert into VENDA_ITENS'
      '  (ID_VENDA, ID_PRODUTO, QTDE, VLR_ITEM, VLT_TOT_ITEM)'
      'values'
      '  (:ID_VENDA, :ID_PRODUTO, :QTDE, :VLR_ITEM, :VLT_TOT_ITEM)')
    SelectSQL.Strings = (
      'Select * From VENDA_ITENS')
    ModifySQL.Strings = (
      'update VENDA_ITENS'
      'set'
      '  ID_VENDA = :ID_VENDA,'
      '  ID_PRODUTO = :ID_PRODUTO,'
      '  QTDE = :QTDE,'
      '  VLR_ITEM = :VLR_ITEM,'
      '  VLT_TOT_ITEM = :VLT_TOT_ITEM'
      'where'
      '  ID_VENDA = :OLD_ID_VENDA and'
      '  ID_PRODUTO = :OLD_ID_PRODUTO')
    Left = 262
    Top = 72
    object qryVendaItensID_VENDA: TIntegerField
      FieldName = 'ID_VENDA'
      Origin = 'VENDA_ITENS.ID_VENDA'
      Required = True
    end
    object qryVendaItensID_PRODUTO: TIntegerField
      FieldName = 'ID_PRODUTO'
      Origin = 'VENDA_ITENS.ID_PRODUTO'
      Required = True
    end
    object qryVendaItensQTDE: TIBBCDField
      FieldName = 'QTDE'
      Origin = 'VENDA_ITENS.QTDE'
      Precision = 9
      Size = 2
    end
    object qryVendaItensVLR_ITEM: TIBBCDField
      FieldName = 'VLR_ITEM'
      Origin = 'VENDA_ITENS.VLR_ITEM'
      Precision = 9
      Size = 2
    end
    object qryVendaItensVLT_TOT_ITEM: TIBBCDField
      FieldName = 'VLT_TOT_ITEM'
      Origin = 'VENDA_ITENS.VLT_TOT_ITEM'
      Precision = 9
      Size = 2
    end
  end
  object dsVendaCab: TDataSource
    DataSet = qryVendaCab
    Left = 185
    Top = 72
  end
  object dsVendaItens: TDataSource
    DataSet = qryVendaItens
    Left = 331
    Top = 72
  end
end
