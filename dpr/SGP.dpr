program SGP;

uses
  Forms,
  unPrincipal in '..\pas\unPrincipal.pas' {frmPrincipal},
  unCadCliente in '..\pas\unCadCliente.pas' {frmCadCliente},
  unCadProduto in '..\pas\unCadProduto.pas' {frmCadProduto},
  unCadMarca in '..\pas\unCadMarca.pas' {frmCadMarca},
  unDmPedidos in '..\pas\unDmPedidos.pas' {dmPedidos: TDataModule},
  unVendaProdutos in '..\pas\unVendaProdutos.pas' {frmVendaProdutos},
  unConsultaProdutos in '..\pas\unConsultaProdutos.pas' {frmConsultaProduto},
  unConfigSistema in '..\pas\unConfigSistema.pas' {frmConfigSistema},
  unSobre in '..\pas\unSobre.pas' {frmSobre},
  unRelClientes in '..\pas\unRelClientes.pas' {frmRelClientes},
  unRelProdutos in '..\pas\unRelProdutos.pas' {frmRelProdutos},
  unRelVendas in '..\pas\unRelVendas.pas' {frmRelVendas},
  unConexao in '..\pas\unConexao.pas' {frmConexao};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'SGP - Sistema de Gerencimaneto de Pedidos';
  Application.CreateForm(TdmPedidos, dmPedidos);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.Run;
end.
