unit unCadCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, StdCtrls, ComCtrls, Buttons, DBCtrls,
  Mask, DB;

type
  TfrmCadCliente = class(TForm)
    pnbotoes: TPanel;
    pnCorpo: TPanel;
    pgcAcoes: TPageControl;
    TabPesquisa: TTabSheet;
    TabCadastro: TTabSheet;
    pnPesquisa: TPanel;
    pnGrid: TPanel;
    rgPesquisa: TRadioGroup;
    GroupBox1: TGroupBox;
    edtLocalizar: TEdit;
    btnPesquisar: TSpeedButton;
    GridCliente: TDBGrid;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    btnNovo: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnExcluir: TSpeedButton;
    btnFechar: TSpeedButton;
    lb1: TLabel;
    edtID: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    rgTpPessoa: TRadioGroup;
    cbUF: TDBComboBox;
    lb7: TLabel;
    bvl1: TBevel;
    procedure btnNovoClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure pgcAcoesChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnFecharClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure GridClienteDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure EnableButtons(Value : Boolean; State : String);
  public
    { Public declarations }
  end;

var
  frmCadCliente: TfrmCadCliente;

implementation

{$R *.dfm}

uses unDmPedidos;

{ TfrmCadCliente }

procedure TfrmCadCliente.EnableButtons(Value: Boolean; State: String);
var
  I : Integer;
 begin
  with pnBotoes do
   for I := ControlCount -1 downto 0 do
    Controls[I].Enabled := Value;
   if (State = 'Insert') then
    pnBotoes.Caption := 'Cadastro de Cliente - Inserindo'
   else
   if (State = 'Edit') then
    pnBotoes.Caption := 'Cadastro de Cliente - Alterando'
   else
    pnBotoes.Caption := '';
 end;

procedure TfrmCadCliente.btnNovoClick(Sender: TObject);
 begin
  EnableButtons(False, 'Insert');
  pgcAcoes.ActivePage := tabCadastro;
  with dmPedidos do
   begin
    try
     if (qryCliente.Active = False) then
      qryCliente.Open;
     qryCliente.Insert;
     qryClienteDT_CADASTRO.AsDateTime := Date;
     qryClienteDT_ALTERACAO.AsDateTime := Date;
    except
     on e : Exception do
      ShowMessage('Erro ao tentar colocar a tabela em modo de inser��o!' + e.Message);
    end
   end;
 end;

procedure TfrmCadCliente.btnAlterarClick(Sender: TObject);
 begin
  if (GridCliente.DataSource.DataSet.RecordCount = 0) then
   begin
    Application.MessageBox('Selecione um registro antes de tentar realizar uma altera��o!','Sem Dados para Alterar',
                             MB_OK + MB_ICONWARNING);
    Abort;
   end;

  EnableButtons(False, 'Edit');
  pgcAcoes.ActivePage := tabCadastro;

  with dmPedidos do
   begin
    try
     if (qryCliente.Active = False) then
      qryCliente.Open;
     qryCliente.Edit;
     if (qryClienteTP_PESSOA.AsString = 'F') then
      rgTpPessoa.ItemIndex := 0
     else
      rgTpPessoa.ItemIndex := 1;
     qryClienteDT_ALTERACAO.AsDateTime := Date;
    except
     on e : Exception do
      ShowMessage('Erro ao tentar colocar a tabela em modo de edi��o!' + e.Message);
    end
   end;
 end;

procedure TfrmCadCliente.btnConfirmarClick(Sender: TObject);
 begin
  EnableButtons(True, '');
  pgcAcoes.ActivePage := tabPesquisa;
  with dmPedidos do
   try
    if (rgTpPessoa.ItemIndex = 0) then
     qryClienteTP_PESSOA.AsString := 'F'
    else
     qryClienteTP_PESSOA.AsString := 'J';
    qryCliente.Post;
    qryCliente.ApplyUpdates;
    Application.MessageBox('Cliente Gravado com Sucesso!', 'Sucesso', MB_OK + MB_ICONINFORMATION);
    qryCliente.Close;
   except
    On e : Exception do
     begin
      ShowMessage('Erro ao tentar gravar dados na tabela de Cliente! ' + e.Message);
      qryCliente.Cancel;
      qryCliente.CancelUpdates;
     end;
   end;
 end;

procedure TfrmCadCliente.btnCancelarClick(Sender: TObject);
 begin
  EnableButtons(True, '');
  pgcAcoes.ActivePage := tabPesquisa;
  with dmPedidos do
   try
    qryCliente.Cancel;
    qryCliente.CancelUpdates;
    qryCliente.Close;
   except
    On e : Exception do
     begin
      ShowMessage('Erro ao tentar cancelar uma opera��o na tabela de Cliente!' + e.Message);
      qryCliente.Cancel;
      qryCliente.CancelUpdates;
     end;
   end;
 end;

procedure TfrmCadCliente.FormActivate(Sender: TObject);
 begin
  //Para garantir que independente da aba que o EXE for criado o form sempre abrir� na aba de pesquisa -- Junior Lira 07/09/2018
  pgcAcoes.ActivePage := tabPesquisa;
 end;

procedure TfrmCadCliente.pgcAcoesChanging(Sender: TObject;
  var AllowChange: Boolean);
 begin
  //Bloqueando a Navega��o entre as abas pelo click -- Junior Lira 07/09/2018
  AllowChange := False;
 end;

procedure TfrmCadCliente.FormKeyPress(Sender: TObject; var Key: Char);
 begin
  //Tab Order pelo Enter -- Junior Lira 07/09/2018
  if (Key = #13) then
   begin
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);
   end;
 end;

procedure TfrmCadCliente.btnPesquisarClick(Sender: TObject);
 begin
  with dmPedidos do
   try
    if (edtLocalizar.Text <> '') then
     begin
      if (rgPesquisa.ItemIndex = 0) then
       begin
        qryCliente.Close;
        qryCliente.SelectSQL.Clear;
        qryCliente.SelectSQL.Add('Select * From CLIENTE Where ID = ' + QuotedStr(edtLocalizar.Text));
        qryCliente.Open;
       end
      else if (rgPesquisa.ItemIndex = 1) then
       begin
        qryCliente.Close;
        qryCliente.SelectSQL.Clear;
        qryCliente.SelectSQL.Add('Select * From CLIENTE Where NM_FANTASIA like ' + QuotedStr('%' + edtLocalizar.Text + '%'));
        qryCliente.Open;
       end
      else if (rgPesquisa.ItemIndex = 2) then
       begin
        qryCliente.Close;
        qryCliente.SelectSQL.Clear;
        qryCliente.SelectSQL.Add('Select * From CLIENTE Where RZ_SOCIAL like ' + QuotedStr('%' + edtLocalizar.Text + '%'));
        qryCliente.Open;
       end
      else
       begin
        qryCliente.Close;
        qryCliente.SelectSQL.Clear;
        qryCliente.SelectSQL.Add('Select * From CLIENTE Where DOCUMENTO like ' + QuotedStr('%' + edtLocalizar.Text + '%'));
        qryCliente.Open;
       end;

      if (qryCliente.IsEmpty) then
       Application.MessageBox('Nenhum resultado encontrado para a pesquisa realizada!','Pesquisa sem Resultado',
                             MB_OK + MB_ICONWARNING);
     end
    else
     begin
       Application.MessageBox('Informe um par�metro antes de tentar realizar a pesquisa!','Sem Dados para Pesquisar',
                             MB_OK + MB_ICONWARNING);
     end;
   except
    On e : Exception do
     begin
      ShowMessage('Erro ao tentar realizar uma pesquisa na tabela de Cliente!' + e.Message);
      qryCliente.Cancel;
      qryCliente.CancelUpdates;
     end;
   end;
 end;

procedure TfrmCadCliente.FormClose(Sender: TObject;
  var Action: TCloseAction);
 begin
  if (dmPedidos.qryCliente.State = dsInsert) or (dmPedidos.qryCliente.State = dsEdit) then
   begin
    Application.MessageBox('Registro em modo de inser��o ou edi��o, salve ou cancele as altera��es!','Aviso',
                             MB_OK + MB_ICONWARNING);
    Action := caNone;
   end
  else
   begin
    dmPedidos.qryCliente.Close;
    Action := caFree;
   end;
 end;

procedure TfrmCadCliente.btnFecharClick(Sender: TObject);
 begin
  Close;
 end;

procedure TfrmCadCliente.btnExcluirClick(Sender: TObject);
 begin
  if (GridCliente.DataSource.DataSet.RecordCount = 0) then
   begin
    Application.MessageBox('N�o existe clientes para serem excluidos!','Tabela Vazia',
                             MB_OK + MB_ICONWARNING);
    Abort;
   end;
  with dmPedidos do
   Begin
    if (GridCliente.Focused) then
     begin
      if (Application.MessageBox('Deseja realmente excluir o item selecionado?','Apagar Registro',MB_YESNO+MB_ICONQUESTION) = idYES) then
       begin
        try
         qryCliente.Delete;
         qryCliente.ApplyUpdates;
         Application.MessageBox('Cliente Excluido com Sucesso!', 'Sucesso', MB_OK + MB_ICONINFORMATION);
        except
         on E : Exception do
          begin
           ShowMessage('Erro ao apagar registro na tabela!' + E.Message);
           qryCliente.Cancel;
           qryCliente.cancelUpdates;
          end;
        end;
       end;
     end
    else
     begin
      Application.MessageBox('Selecione o cliente antes de continuar!','Escolher item',MB_OK + MB_ICONWARNING);
      Abort;
     end; 
   end;
 end;

procedure TfrmCadCliente.GridClienteDblClick(Sender: TObject);
 begin
  btnAlterar.Click;
 end;

end.
