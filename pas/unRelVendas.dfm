object frmRelVendas: TfrmRelVendas
  Left = 455
  Top = 187
  BorderStyle = bsDialog
  Caption = 'Relat'#243'rio de Vendas'
  ClientHeight = 283
  ClientWidth = 358
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    358
    283)
  PixelsPerInch = 96
  TextHeight = 16
  object bvl1: TBevel
    Left = 5
    Top = 6
    Width = 347
    Height = 240
    Shape = bsFrame
    Style = bsRaised
  end
  object lb3: TLabel
    Left = 12
    Top = 13
    Width = 160
    Height = 18
    Caption = 'Vendidas ao Cliente'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dblCliente: TDBLookupComboBox
    Left = 12
    Top = 34
    Width = 335
    Height = 22
    Ctl3D = False
    KeyField = 'ID'
    ListField = 'NM_FANTASIA'
    ListSource = dmPedidos.dsCliente
    ParentCtl3D = False
    TabOrder = 0
  end
  object rg2: TGroupBox
    Left = 12
    Top = 65
    Width = 335
    Height = 105
    Caption = 'Realizadas no Per'#237'odo:'
    TabOrder = 1
    object lb1: TLabel
      Left = 16
      Top = 43
      Width = 28
      Height = 18
      Caption = 'De:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb2: TLabel
      Left = 173
      Top = 43
      Width = 35
      Height = 18
      Caption = 'At'#233':'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object dtpDtInicial: TDateTimePicker
      Left = 16
      Top = 67
      Width = 145
      Height = 24
      BevelInner = bvSpace
      BevelOuter = bvSpace
      BevelKind = bkFlat
      Date = 43351.962394525460000000
      Time = 43351.962394525460000000
      Color = clBtnFace
      Enabled = False
      TabOrder = 0
    end
    object dtpDtFinal: TDateTimePicker
      Left = 173
      Top = 67
      Width = 145
      Height = 24
      BevelInner = bvSpace
      BevelOuter = bvSpace
      BevelKind = bkFlat
      Date = 43351.962479421290000000
      Time = 43351.962479421290000000
      Color = clBtnFace
      Enabled = False
      TabOrder = 1
    end
    object cbFiltroDatas: TCheckBox
      Left = 16
      Top = 24
      Width = 193
      Height = 17
      Caption = 'Usar Filtro de Datas'
      TabOrder = 2
      OnClick = cbFiltroDatasClick
    end
  end
  object rgTipoVenda: TRadioGroup
    Left = 12
    Top = 178
    Width = 335
    Height = 59
    Align = alCustom
    Caption = 'Tipo de Venda:'
    Columns = 3
    ItemIndex = 2
    Items.Strings = (
      'A Vista'
      'A Prazo'
      'Todas')
    TabOrder = 2
  end
  object btnConfirmar: TBitBtn
    Left = 109
    Top = 252
    Width = 120
    Height = 28
    Anchors = [akLeft, akBottom]
    Caption = '&Confirmar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 3
    OnClick = btnConfirmarClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object btnCancelar: TBitBtn
    Left = 232
    Top = 252
    Width = 120
    Height = 28
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Ca&ncelar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 4
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object frxRelVenda: TfrxReport
    Version = '6.0.10'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43351.968657303200000000
    ReportOptions.LastChange = 43351.968657303200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 40
    Top = 248
    Datasets = <
      item
        DataSet = frxDBVenda
        DataSetName = 'frxDBVenda'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Calibri'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Color = clWhite
      Frame.Typ = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 52.913420000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 3.779527560000000000
          Top = 1.889763780000000000
          Width = 272.126160000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8 = (
            'Rela'#195#167#195#163'o de Vendas Realizadas')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 23.897650000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Date: TfrxMemoView
          IndexTag = 1
          Left = 457.543600000000000000
          Top = 25.456710000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8 = (
            'P'#195#161'gina Impressa em: [Date]  -  [Time]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Line2: TfrxLineView
          Top = 45.354360000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 94.488250000000000000
        Width = 718.110700000000000000
        object Memo3: TfrxMemoView
          Left = 3.779527560000000000
          Width = 71.811023622047200000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWindow
          HAlign = haCenter
          Memo.UTF8 = (
            'C'#195#179'digo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 80.252010000000000000
          Width = 117.165283540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWindow
          HAlign = haCenter
          Memo.UTF8 = (
            'Tipo Venda')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 201.417440000000000000
          Width = 177.637795275591000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWindow
          HAlign = haCenter
          Memo.UTF8 = (
            'Data e Hora Venda')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 400.071120000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWindow
          HAlign = haCenter
          Memo.UTF8 = (
            'Vlr. Bruto ')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 512.779840000000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWindow
          HAlign = haCenter
          Memo.UTF8 = (
            'Vrl. Desconto')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 620.047620000000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWindow
          HAlign = haCenter
          Memo.UTF8 = (
            'Vlr. Liquido')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = -0.220470000000000000
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 185.196970000000000000
        Width = 718.110700000000000000
        DataSet = frxDBVenda
        DataSetName = 'frxDBVenda'
        RowCount = 0
        object frxDBClienteID: TfrxMemoView
          IndexTag = 1
          Left = 3.779527560000000000
          Width = 71.811023622047200000
          Height = 18.897650000000000000
          DataField = 'ID'
          DataSet = frxDBVenda
          DataSetName = 'frxDBVenda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDBVenda."ID"]')
          ParentFont = False
        end
        object frxDBClienteDOCUMENTO: TfrxMemoView
          IndexTag = 1
          Left = 201.448818900000000000
          Width = 177.637795280000000000
          Height = 18.897650000000000000
          DataSet = frxDBVenda
          DataSetName = 'frxDBVenda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDBVenda."DT_VENDA"] - [frxDBVenda."HR_VENDA"]')
          ParentFont = False
        end
        object frxDBClienteTP_PESSOA: TfrxMemoView
          IndexTag = 1
          Left = 396.472458030000000000
          Width = 105.826783860000000000
          Height = 18.897650000000000000
          DataSet = frxDBVenda
          DataSetName = 'frxDBVenda'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBVenda."VLR_TOT_VENDA"]')
          ParentFont = False
        end
        object frxDBClienteCELULAR: TfrxMemoView
          IndexTag = 1
          Left = 518.929175350000000000
          Width = 79.370078740000000000
          Height = 18.897650000000000000
          DataSet = frxDBVenda
          DataSetName = 'frxDBVenda'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBVenda."VLR_DESC_REAL"]')
          ParentFont = False
        end
        object frxDBClienteCIDADE: TfrxMemoView
          IndexTag = 1
          Left = 619.086677640000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          DataSet = frxDBVenda
          DataSetName = 'frxDBVenda'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBVenda."VLR_LIQ_VENDA"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 80.125984250000000000
          Width = 117.165354330000000000
          Height = 18.897650000000000000
          DataSet = frxDBVenda
          DataSetName = 'frxDBVenda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[(IIF(<frxDBVenda."TP_VENDA">='#39'V'#39','#39'A VISTA'#39','#39'A PRAZO'#39'))]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 377.953000000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 2.779527560000000000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8 = (
            'Desenvolvido por: J'#195#186'nior Lira')
          ParentFont = False
        end
        object TotalPages: TfrxMemoView
          IndexTag = 1
          Left = 533.338900000000000000
          Top = 2.779527560000000000
          Width = 181.417440000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8 = (
            'P'#195#161'gina [Page] de [TotalPages#]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 1.133858267716540000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Frame.Typ = []
        Height = 86.929190000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        object Line5: TfrxLineView
          Top = 0.338590000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object COUNT: TfrxMemoView
          IndexTag = 1
          Left = 3.118120000000000000
          Top = 3.000000000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'N'#194#186' de Registros:  [COUNT(MasterData1)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 291.023810000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Totalizadores:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          IndexTag = 1
          Left = 396.850650000000000000
          Top = 2.559060000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDBVenda."VLR_TOT_VENDA">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          IndexTag = 1
          Left = 517.795610000000000000
          Top = 3.779530000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDBVenda."VLR_DESC_REAL">)]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          IndexTag = 1
          Left = 619.842920000000000000
          Top = 3.779530000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDBVenda."VLR_LIQ_VENDA">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDBVenda: TfrxDBDataset
    UserName = 'frxDBVenda'
    CloseDataSource = False
    DataSet = dmPedidos.qryVendaCab
    BCDToCurrency = False
    Left = 72
    Top = 248
  end
end
