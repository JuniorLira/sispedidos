unit unCadProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids, ExtCtrls, ComCtrls, DBCtrls,
  Mask, DB;

type
  TfrmCadProduto = class(TForm)
    pnCorpo: TPanel;
    pgcAcoes: TPageControl;
    TabPesquisa: TTabSheet;
    pnPesquisa: TPanel;
    rgPesquisa: TRadioGroup;
    GroupBox1: TGroupBox;
    pnGrid: TPanel;
    GridProduto: TDBGrid;
    TabCadastro: TTabSheet;
    pnbotoes: TPanel;
    btnNovo: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnExcluir: TSpeedButton;
    btnFechar: TSpeedButton;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    edtLocalizar: TEdit;
    btnPesquisar: TSpeedButton;
    lb1: TLabel;
    edtID: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    dblMarca: TDBLookupComboBox;
    lb2: TLabel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    Label9: TLabel;
    DBEdit8: TDBEdit;
    bvl1: TBevel;
    cbUnidade: TDBComboBox;
    procedure btnNovoClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure pgcAcoesChanging(Sender: TObject; var AllowChange: Boolean);
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnFecharClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnExcluirClick(Sender: TObject);
    procedure GridProdutoDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure EnableButtons(Value : Boolean; State : String);
  end;

var
  frmCadProduto: TfrmCadProduto;

implementation

{$R *.dfm}

uses unDmPedidos;

{ TfrmCadProduto }

procedure TfrmCadProduto.EnableButtons(Value: Boolean; State: String);
var
  I : Integer;
 begin
   with pnBotoes do
   for I := ControlCount -1 downto 0 do
    Controls[I].Enabled := Value;
   if (State = 'Insert') then
    pnBotoes.Caption := 'Cadastro de Produto - Inserindo'
   else
   if (State = 'Edit') then
    pnBotoes.Caption := 'Cadastro de Produto - Alterando'
   else
    pnBotoes.Caption := '';
 end;
procedure TfrmCadProduto.btnNovoClick(Sender: TObject);
 begin
  EnableButtons(False, 'Insert');
  pgcAcoes.ActivePage := tabCadastro;
  with dmPedidos do
   begin
    try
     if (qryProduto.Active = False) then
      qryProduto.Open;
     if (qryMarca.Active = False) then
      qryMarca.Open;
     qryProduto.Insert;
     qryProdutoDT_CADASTRO.AsDateTime := Date;
     qryProdutoDT_ALTERACAO.AsDateTime := Date;
    except
     on e : Exception do
      ShowMessage('Erro ao tentar colocar a tabela em modo de inser��o!' + e.Message);
    end
   end;
 end;

procedure TfrmCadProduto.btnAlterarClick(Sender: TObject);
 begin
  if (GridProduto.DataSource.DataSet.RecordCount = 0) then
   begin
    Application.MessageBox('Selecione um registro antes de tentar realizar uma altera��o!','Sem Dados para Alterar',
                             MB_OK + MB_ICONWARNING);
    Abort;
   end;

  EnableButtons(False, 'Edit');
  pgcAcoes.ActivePage := tabCadastro;

  with dmPedidos do
   begin
    try
     if (qryProduto.Active = False) then
      qryProduto.Open;
     if (qryMarca.Active = False) then
      qryMarca.Open;
     qryProduto.Edit;   
     qryProdutoDT_ALTERACAO.AsDateTime := Date;
    except
     on e : Exception do
      ShowMessage('Erro ao tentar colocar a tabela em modo de edi��o!' + e.Message);
    end
   end;
 end;

procedure TfrmCadProduto.btnConfirmarClick(Sender: TObject);
 begin
  if (dblMarca.KeyValue = null) then
   begin
    Application.MessageBox('Selecione uma marca para o produto antes de salvar o cadastro!','Produto sem Marca',
                             MB_OK + MB_ICONWARNING);
    dblMarca.SetFocus;
    Abort;
   end;
  EnableButtons(True, '');
  pgcAcoes.ActivePage := tabPesquisa;
  with dmPedidos do
   try                             
    qryProduto.Post;
    qryProduto.ApplyUpdates;
    Application.MessageBox('Produto Gravado com Sucesso!', 'Sucesso', MB_OK + MB_ICONINFORMATION);
    qryProduto.Close;
    qryMarca.Close;
   except
    On e : Exception do
     begin
      ShowMessage('Erro ao tentar gravar dados na tabela de Produto! ' + e.Message);
      qryProduto.Cancel;
      qryProduto.CancelUpdates;
     end;
   end;
 end;

procedure TfrmCadProduto.btnCancelarClick(Sender: TObject);
 begin
  EnableButtons(True, '');
  pgcAcoes.ActivePage := tabPesquisa;
  with dmPedidos do
   try
    qryProduto.Cancel;
    qryProduto.CancelUpdates;
    qryProduto.Close;
   except
    On e : Exception do
     begin
      ShowMessage('Erro ao tentar cancelar uma opera��o na tabela de Marca!' + e.Message);
      qryProduto.Cancel;
      qryProduto.CancelUpdates;
     end;
   end;
 end;

procedure TfrmCadProduto.FormActivate(Sender: TObject);
 begin
  //Para garantir que independente da aba que o EXE for criado o form sempre abrir� na aba de pesquisa -- Junior Lira 07/09/2018
  pgcAcoes.ActivePage := tabPesquisa;
 end;

procedure TfrmCadProduto.pgcAcoesChanging(Sender: TObject;
  var AllowChange: Boolean);
 begin
  //Bloqueando a Navega��o entre as abas pelo click -- Junior Lira 07/09/2018
  AllowChange := False;
 end;

procedure TfrmCadProduto.btnPesquisarClick(Sender: TObject);
 begin
  with dmPedidos do
   try
    if (edtLocalizar.Text <> '') then
     begin
      if (rgPesquisa.ItemIndex = 0) then
       begin
        qryProduto.Close;
        qryProduto.SelectSQL.Clear;
        qryProduto.SelectSQL.Add('Select * From PRODUTO Where ID = ' + QuotedStr(edtLocalizar.Text));
        qryProduto.Open;
       end
      else if (rgPesquisa.ItemIndex = 1) then
       begin
        qryProduto.Close;
        qryProduto.SelectSQL.Clear;
        qryProduto.SelectSQL.Add('Select * From PRODUTO Where DECRICAO like ' + QuotedStr('%' + edtLocalizar.Text + '%'));
        qryProduto.Open;
       end
      else
       begin
        qryProduto.Close;
        qryProduto.SelectSQL.Clear;
        qryProduto.SelectSQL.Add('Select * From PRODUTO Where COD_BARRA like ' + QuotedStr('%' + edtLocalizar.Text + '%'));
        qryProduto.Open;
       end;

      if (qryProduto.IsEmpty) then
       Application.MessageBox('Nenhum resultado encontrado para a pesquisa realizada!','Pesquisa sem Resultado',
                             MB_OK + MB_ICONWARNING);
     end
    else
     begin
       Application.MessageBox('Informe um par�metro antes de tentar realizar a pesquisa!','Sem Dados para Pesquisar',
                             MB_OK + MB_ICONWARNING);
     end;
   except
    On e : Exception do
     begin
      ShowMessage('Erro ao tentar realizar uma pesquisa na tabela de Produto!' + e.Message);
      qryProduto.Cancel;
      qryProduto.CancelUpdates;
     end;
   end;
 end;

procedure TfrmCadProduto.FormClose(Sender: TObject;
  var Action: TCloseAction);
 begin
  if (dmPedidos.qryProduto.State = dsInsert) or (dmPedidos.qryProduto.State = dsEdit) then
   begin
    Application.MessageBox('Registro em modo de inser��o ou edi��o, salve ou cancele as altera��es!','Aviso',
                             MB_OK + MB_ICONWARNING);
    Action := caNone;
   end
  else
   begin
    dmPedidos.qryProduto.Close;
    dmPedidos.qryMarca.Close;
    Action := caFree;
   end;
 end;

procedure TfrmCadProduto.btnFecharClick(Sender: TObject);
 begin
  Close;
 end;

procedure TfrmCadProduto.FormKeyPress(Sender: TObject; var Key: Char);
 begin
  if (Key = #13) then
   begin
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);
   end;
 end;

procedure TfrmCadProduto.btnExcluirClick(Sender: TObject);
 begin
  if (GridProduto.DataSource.DataSet.RecordCount = 0) then
   begin
    Application.MessageBox('N�o existe produtos para serem excluidos!','Tabela Vazia',
                             MB_OK + MB_ICONWARNING);
    Abort;
   end;
  with dmPedidos do
   Begin
    if (GridProduto.Focused) then
     begin
      if (Application.MessageBox('Deseja realmente excluir o item selecionado?','Apagar Registro',MB_YESNO+MB_ICONQUESTION) = idYES) then
       begin
        try
         qryProduto.Delete;
         qryProduto.ApplyUpdates;
         Application.MessageBox('Produto Excluido com Sucesso!', 'Sucesso', MB_OK + MB_ICONINFORMATION);
        except
         on E : Exception do
          begin
           ShowMessage('Erro ao apagar registro na tabela!' + E.Message);
           qryProduto.Cancel;
           qryProduto.cancelUpdates;
          end;
        end;
       end;
     end
    else
     begin
      Application.MessageBox('Selecione o produto antes de continuar!','Escolher item',MB_OK + MB_ICONWARNING);
      Abort;
     end; 
   end;
 end;

procedure TfrmCadProduto.GridProdutoDblClick(Sender: TObject);
 begin
  btnAlterar.Click;
 end;

end.
