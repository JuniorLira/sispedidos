unit unRelClientes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Buttons, frxClass, frxDBSet;

type
  TfrmRelClientes = class(TForm)
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    rgTipoPessoa: TRadioGroup;
    rg2: TGroupBox;
    dtpDtInicial: TDateTimePicker;
    dtpDtFinal: TDateTimePicker;
    lb1: TLabel;
    lb2: TLabel;
    frxRelCliente: TfrxReport;
    frxDBCliente: TfrxDBDataset;
    cbFiltroDatas: TCheckBox;
    procedure btnConfirmarClick(Sender: TObject);
    procedure cbFiltroDatasClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelClientes: TfrmRelClientes;

implementation

{$R *.dfm}

uses unDmPedidos;

procedure TfrmRelClientes.btnConfirmarClick(Sender: TObject);
 begin
  with dmPedidos do
   begin
    qryCliente.Close;
    qryCliente.SelectSQL.Clear;
    qryCliente.SelectSQL.Add('Select * From CLIENTE ');
    if (rgTipoPessoa.ItemIndex <> 2) then
     begin
      case rgTipoPessoa.ItemIndex of
       0 :
        qryCliente.SelectSQL.Add('WHERE TP_PESSOA = ' + QuotedStr('F'));
       1 :
        qryCliente.SelectSQL.Add('WHERE TP_PESSOA = ' + QuotedStr('J'));
      end;
      if (cbFiltroDatas.Checked) then
       qryCliente.SelectSQL.Add(' AND DT_CADASTRO >= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtInicial.DateTime)) +
                                ' AND DT_CADASTRO <= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtFinal.DateTime)));
     end
    else
    if (cbFiltroDatas.Checked) then
     begin
      qryCliente.SelectSQL.Add(' WHERE DT_CADASTRO >= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtInicial.DateTime)) +
                                ' AND DT_CADASTRO <= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtFinal.DateTime)));
     end;
    qryCliente.Open;                    
    frxRelCliente.PrepareReport(True);
    frxRelCliente.ShowPreparedReport;
   end;
 end;

procedure TfrmRelClientes.cbFiltroDatasClick(Sender: TObject);
 begin
  if cbFiltroDatas.Checked then
   begin
    dtpDtInicial.Color   := clWindow;
    dtpDtFinal.Color     := clWindow;
    dtpDtInicial.Enabled := True;
    dtpDtFinal.Enabled   := True;
   end
  else
   begin
    dtpDtInicial.Color   := clBtnFace;
    dtpDtFinal.Color     := clBtnFace;
    dtpDtInicial.Enabled := False;
    dtpDtFinal.Enabled   := False;
   end;
 end;

procedure TfrmRelClientes.FormClose(Sender: TObject;
  var Action: TCloseAction);
 begin
  dmPedidos.qryCliente.Close;
 end;

end.
