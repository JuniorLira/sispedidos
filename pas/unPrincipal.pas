unit unPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Buttons, ExtCtrls, Menus, IniFiles;

type
  TfrmPrincipal = class(TForm)
    mnPrincipal: TMainMenu;
    Cadastro1: TMenuItem;
    Cliente1: TMenuItem;
    N1: TMenuItem;
    Produto1: TMenuItem;
    Marca1: TMenuItem;
    Pedido1: TMenuItem;
    Venda1: TMenuItem;
    Relatrio1: TMenuItem;
    Ajuda1: TMenuItem;
    SobreoSistema1: TMenuItem;
    Sair1: TMenuItem;
    SairdoSistema1: TMenuItem;
    pnBotoes: TPanel;
    btnCliente: TSpeedButton;
    btnProduto: TSpeedButton;
    btnVenda: TSpeedButton;
    btnSobre: TSpeedButton;
    btnSair: TSpeedButton;
    stbPedidos: TStatusBar;
    mnConfiguraesdoSistema1: TMenuItem;
    btnConfiguracoes: TSpeedButton;
    mnRelaodeClientes1: TMenuItem;
    mnRelaodeProdutos1: TMenuItem;
    mnRelaodeVendas1: TMenuItem;
    mnN2: TMenuItem;
    mnN3: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSairClick(Sender: TObject);
    procedure SairdoSistema1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnClienteClick(Sender: TObject);
    procedure Cliente1Click(Sender: TObject);
    procedure btnProdutoClick(Sender: TObject);
    procedure Produto1Click(Sender: TObject);
    procedure Marca1Click(Sender: TObject);
    procedure btnVendaClick(Sender: TObject);
    procedure btnConfiguracoesClick(Sender: TObject);
    procedure mnConfiguraesdoSistema1Click(Sender: TObject);
    procedure Venda1Click(Sender: TObject);
    procedure btnSobreClick(Sender: TObject);
    procedure SobreoSistema1Click(Sender: TObject);
    procedure mnRelaodeClientes1Click(Sender: TObject);
    procedure mnRelaodeProdutos1Click(Sender: TObject);
    procedure mnRelaodeVendas1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

uses unCadCliente, unCadProduto, unCadMarca, unVendaProdutos, unConfigSistema, unSobre,
     unRelClientes, unRelProdutos, unRelVendas, unDmPedidos, unConexao;

procedure TfrmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
 begin
  if (Application.MessageBox('Deseja realmente sair do Sistema?','Encerrar Aplica��o', MB_YESNO + MB_ICONQUESTION) <> idYes) then
   Action := caNone;
 end;

procedure TfrmPrincipal.btnSairClick(Sender: TObject);
 begin
  Close;
 end;

procedure TfrmPrincipal.SairdoSistema1Click(Sender: TObject);
 begin
  Close;
 end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
var
  IniFile : TIniFile;
  fCaminhoBanco, FileConnection : String;
 begin
  FileConnection := '';
  IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini');
  try
   fCaminhoBanco := IniFile.ReadString('BANCO', 'Caminho', '');
  finally
   IniFile.Free;
  end;

  dmPedidos.Banco.DatabaseName := fCaminhoBanco;

  try
   dmPedidos.Banco.Connected := True;
  except
   repeat
     Application.MessageBox('Aten��o n�o foi poss�vel conectar base de dados. Informe '+
                                 'o caminho do banco manualmente!','Sem Conexao de Dados', MB_OK
                                 + MB_ICONWARNING);
     ShowConexao(FileConnection);
     dmPedidos.Banco.DatabaseName := FileConnection;
     try
      dmPedidos.Banco.Connected := True;
      IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini');
      try
       IniFile.WriteString('BANCO', 'Caminho', FileConnection);
      finally
       IniFile.Free;
      end;
     except
     end;     
   until (dmPedidos.Banco.TestConnected);
  end;

  Caption := Application.Title;
  stbPedidos.Panels[0].Text := Application.Title;
  stbPedidos.Panels[1].Text := 'Local do Execut�vel: ' + Application.ExeName;
  stbPedidos.Panels[2].Text := 'Local do Banco: ' + dmPedidos.Banco.DatabaseName;
  if dmPedidos.Banco.Connected then
   stbPedidos.Panels[3].Text := 'Status da Conex�o: Conectado'
  else
   stbPedidos.Panels[3].Text := 'Status da Conex�o: Desconectado';
  stbPedidos.Panels[4].Text := 'Vers�o: Alfa';
  stbPedidos.Panels[5].Text := 'Data: ' + DateToStr(Date);
  stbPedidos.Panels[6].Text := 'Desenvolvido Por: J�nior Lira';
 end;

procedure TfrmPrincipal.btnClienteClick(Sender: TObject);
 begin
  Application.CreateForm(TfrmCadCliente, frmCadCliente);
  frmCadCliente.ShowModal;
  FreeAndNil(frmCadCliente);
 end;

procedure TfrmPrincipal.Cliente1Click(Sender: TObject);
 begin
  btnCliente.Click;
 end;

procedure TfrmPrincipal.btnProdutoClick(Sender: TObject);
 begin
  Application.CreateForm(TfrmCadProduto, frmCadProduto);
  frmCadProduto.ShowModal;
  FreeAndNil(frmCadProduto);
 end;

procedure TfrmPrincipal.Produto1Click(Sender: TObject);
 begin
  btnProduto.Click;
 end;

procedure TfrmPrincipal.Marca1Click(Sender: TObject);
 begin
  Application.CreateForm(TfrmCadMarca, frmCadMarca);
  frmCadMarca.ShowModal;
  FreeAndNil(frmCadMarca);
 end;

procedure TfrmPrincipal.btnVendaClick(Sender: TObject);
 begin
  Application.CreateForm(TfrmVendaProdutos, frmVendaProdutos);
  frmVendaProdutos.ShowModal;
  FreeAndNil(frmVendaProdutos);
 end;

procedure TfrmPrincipal.btnConfiguracoesClick(Sender: TObject);
 begin
  Application.CreateForm(TfrmConfigSistema, frmConfigSistema);
  frmConfigSistema.ShowModal;
  FreeAndNil(frmConfigSistema);
 end;

procedure TfrmPrincipal.mnConfiguraesdoSistema1Click(Sender: TObject);
 begin
  btnConfiguracoes.Click;
 end;

procedure TfrmPrincipal.Venda1Click(Sender: TObject);
 begin
  btnVenda.Click;
 end;

procedure TfrmPrincipal.btnSobreClick(Sender: TObject);
 begin
  Application.CreateForm(TfrmSobre, frmSobre);
  frmSobre.ShowModal;
  FreeAndNil(frmSobre);
 end;

procedure TfrmPrincipal.SobreoSistema1Click(Sender: TObject);
 begin
  btnSobre.Click;
 end;

procedure TfrmPrincipal.mnRelaodeClientes1Click(Sender: TObject);
 begin
  Application.CreateForm(TfrmRelClientes, frmRelClientes);
  frmRelClientes.ShowModal;
  FreeAndNil(frmRelClientes);
 end;

procedure TfrmPrincipal.mnRelaodeProdutos1Click(Sender: TObject);
 begin
  Application.CreateForm(TfrmRelProdutos, frmRelProdutos);
  frmRelProdutos.ShowModal;
  FreeAndNil(frmRelProdutos);
 end;

procedure TfrmPrincipal.mnRelaodeVendas1Click(Sender: TObject);
 begin
  Application.CreateForm(TfrmRelVendas, frmRelVendas);
  frmRelVendas.ShowModal;
  FreeAndNil(frmRelVendas);
 end;

end.
