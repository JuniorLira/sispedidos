object frmConfigSistema: TfrmConfigSistema
  Left = 465
  Top = 214
  BorderStyle = bsDialog
  Caption = 'Configura'#231#245'es do Sistema'
  ClientHeight = 249
  ClientWidth = 552
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pn1: TPanel
    Left = 0
    Top = 0
    Width = 552
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    BevelWidth = 2
    Caption = 'Configura'#231#245'es do Sistema'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object pn2: TPanel
    Left = 0
    Top = 211
    Width = 552
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      552
      38)
    object btnConfirmar: TBitBtn
      Left = 305
      Top = 5
      Width = 120
      Height = 28
      Anchors = [akLeft, akBottom]
      Caption = '&Confirmar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Verdana'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      OnClick = btnConfirmarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object btnCancelar: TBitBtn
      Left = 428
      Top = 5
      Width = 120
      Height = 28
      Anchors = [akLeft, akBottom]
      Cancel = True
      Caption = 'Ca&ncelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Verdana'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object pn3: TPanel
    Left = 0
    Top = 41
    Width = 552
    Height = 170
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object pgcConfig: TPageControl
      Left = 1
      Top = 1
      Width = 550
      Height = 168
      ActivePage = tabImpressao
      Align = alClient
      Style = tsFlatButtons
      TabOrder = 0
      object tabEstoque: TTabSheet
        Caption = '&Estoque'
        object cbContEstoque: TCheckBox
          Left = 6
          Top = 6
          Width = 193
          Height = 17
          Caption = '1) Controlar Estoque'
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
        end
        object cbPermVenSemSaldo: TCheckBox
          Left = 6
          Top = 30
          Width = 283
          Height = 17
          Caption = '2) Permitir Vender Produto sem Saldo'
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
        end
      end
      object tabImpressao: TTabSheet
        Caption = '&Impress'#227'o'
        ImageIndex = 1
        object lb1: TLabel
          Left = 6
          Top = 29
          Width = 322
          Height = 16
          Caption = '4) Caminho da Impressora para Impress'#227'o Direta'
        end
        object cbImpresDireta: TCheckBox
          Left = 6
          Top = 6
          Width = 283
          Height = 17
          Caption = '3) Impress'#227'o Direta na Venda'
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          OnClick = cbImpresDiretaClick
        end
        object edtCaminhoImpressora: TEdit
          Left = 6
          Top = 48
          Width = 400
          Height = 22
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
        end
      end
      object tabBancoDados: TTabSheet
        Caption = 'Banco de &Dados'
        ImageIndex = 2
        object lb2: TLabel
          Left = 6
          Top = 6
          Width = 206
          Height = 16
          Caption = '4) Caminho do Banco de Dados'
        end
        object edtCaminhoBanco: TEdit
          Left = 6
          Top = 29
          Width = 400
          Height = 22
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
        end
        object btnLocal: TButton
          Left = 409
          Top = 27
          Width = 47
          Height = 25
          Caption = '...'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = btnLocalClick
        end
      end
    end
  end
  object dlgBase: TOpenDialog
    Filter = 'Arquivos Firebird|*.FDB'
    InitialDir = 'C:\'
    Left = 240
    Top = 136
  end
end
