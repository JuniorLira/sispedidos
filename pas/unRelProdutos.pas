unit unRelProdutos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DBCtrls, StdCtrls, Buttons, ComCtrls, frxClass,
  frxDBSet;

type
  TfrmRelProdutos = class(TForm)
    rg2: TGroupBox;
    lb1: TLabel;
    lb2: TLabel;
    dtpDtInicial: TDateTimePicker;
    dtpDtFinal: TDateTimePicker;
    cbFiltroDatas: TCheckBox;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    dblMarca: TDBLookupComboBox;
    lb3: TLabel;
    cbSaldoPositivo: TCheckBox;
    bvl1: TBevel;
    frxRelProduto: TfrxReport;
    frxDBProduto: TfrxDBDataset;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnConfirmarClick(Sender: TObject);
    procedure cbFiltroDatasClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelProdutos: TfrmRelProdutos;

implementation

{$R *.dfm}

uses unDmPedidos;

procedure TfrmRelProdutos.FormShow(Sender: TObject);
 begin
  dmPedidos.qryMarca.Open;
 end;

procedure TfrmRelProdutos.FormClose(Sender: TObject;
  var Action: TCloseAction);
 begin
  dmPedidos.qryMarca.Close;
 end;

procedure TfrmRelProdutos.btnConfirmarClick(Sender: TObject);
 begin
  with dmPedidos do
   begin
    qryProduto.Close;
    qryProduto.SelectSQL.Clear;
    qryProduto.SelectSQL.Add('Select * From PRODUTO ');
    if (dblMarca.KeyValue <> null) then
     begin
      qryProduto.SelectSQL.Add('WHERE ID_MARCA = ' + QuotedStr(dblMarca.KeyValue));
      if (cbFiltroDatas.Checked) then
       qryProduto.SelectSQL.Add(' AND DT_CADASTRO >= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtInicial.DateTime)) +
                                ' AND DT_CADASTRO <= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtFinal.DateTime)));
      if (cbSaldoPositivo.Checked) then
       qryProduto.SelectSQL.Add(' AND SALDO > 0');
     end
    else
    if (cbFiltroDatas.Checked) then
     begin
      qryProduto.SelectSQL.Add(' WHERE DT_CADASTRO >= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtInicial.DateTime)) +
                                ' AND DT_CADASTRO <= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtFinal.DateTime)));
      if (cbSaldoPositivo.Checked) then
       qryProduto.SelectSQL.Add(' AND SALDO > 0');
     end
    else if (cbSaldoPositivo.Checked) then
     begin
      qryProduto.SelectSQL.Add(' WHERE SALDO > 0');
     end;

    qryProduto.Open;
    frxRelProduto.PrepareReport(True);
    frxRelProduto.ShowPreparedReport;
   end;
 end;

procedure TfrmRelProdutos.cbFiltroDatasClick(Sender: TObject);
 begin
  if cbFiltroDatas.Checked then
   begin
    dtpDtInicial.Color   := clWindow;
    dtpDtFinal.Color     := clWindow;
    dtpDtInicial.Enabled := True;
    dtpDtFinal.Enabled   := True;
   end
  else
   begin
    dtpDtInicial.Color   := clBtnFace;
    dtpDtFinal.Color     := clBtnFace;
    dtpDtInicial.Enabled := False;
    dtpDtFinal.Enabled   := False;
   end;
 end;

end.
