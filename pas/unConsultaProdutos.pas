unit unConsultaProdutos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, Grids, DBGrids, ExtCtrls;

type
  TfrmConsultaProduto = class(TForm)
    pn1: TPanel;
    pn2: TPanel;
    rgTpPesquisa: TRadioGroup;
    rg2: TGroupBox;
    GridProduto: TDBGrid;
    edtLocalizar: TEdit;
    btnPesquisar: TSpeedButton;
    procedure btnPesquisarClick(Sender: TObject);
    procedure GridProdutoDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function ShowConsultaProduto(var CodProd : String) : Boolean;

var
  frmConsultaProduto: TfrmConsultaProduto;

implementation

{$R *.dfm}

uses unDmPedidos, DB;

function ShowConsultaProduto(var CodProd : String) : Boolean;
 begin
  with TfrmConsultaProduto.Create(Application) do
   try
    Result := ShowModal = mrOk;
   finally
    if Result then
     CodProd := dmPedidos.qryProdutoID.AsString;
   end;
 end;

procedure TfrmConsultaProduto.btnPesquisarClick(Sender: TObject);
 begin
  with dmPedidos do
   try
    if (edtLocalizar.Text <> '') then
     begin
      if (rgTpPesquisa.ItemIndex = 0) then
       begin
        qryProduto.Close;
        qryProduto.SelectSQL.Clear;
        qryProduto.SelectSQL.Add('Select * From PRODUTO Where ID = ' + QuotedStr(edtLocalizar.Text));
        qryProduto.Open;
       end
      else if (rgTpPesquisa.ItemIndex = 1) then
       begin
        qryProduto.Close;
        qryProduto.SelectSQL.Clear;
        qryProduto.SelectSQL.Add('Select * From PRODUTO Where DECRICAO like ' + QuotedStr('%' + edtLocalizar.Text + '%'));
        qryProduto.Open;
       end
      else
       begin
        qryProduto.Close;
        qryProduto.SelectSQL.Clear;
        qryProduto.SelectSQL.Add('Select * From PRODUTO Where COD_BARRA like ' + QuotedStr('%' + edtLocalizar.Text + '%'));
        qryProduto.Open;
       end;
     end
    else
     begin
       Application.MessageBox('Informe um par�metro antes de tentar realizar a pesquisa!','Sem Dados para Pesquisar',
                             MB_OK + MB_ICONWARNING);
     end;
   except
    On e : Exception do
     begin
      ShowMessage('Erro ao tentar realizar uma pesquisa na tabela de Produto!' + e.Message);
      qryProduto.Cancel;
      qryProduto.CancelUpdates;
     end;
   end;
 end;

procedure TfrmConsultaProduto.GridProdutoDblClick(Sender: TObject);
 begin
  if not(dmPedidos.qryProduto.IsEmpty) then
   begin
    ModalResult := mrOk;
   end
 end;

end.
