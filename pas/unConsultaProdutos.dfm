object frmConsultaProduto: TfrmConsultaProduto
  Left = 387
  Top = 361
  BorderStyle = bsDialog
  Caption = 'Consulta de Produtos'
  ClientHeight = 207
  ClientWidth = 820
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 18
  object pn1: TPanel
    Left = 0
    Top = 0
    Width = 820
    Height = 65
    Align = alTop
    TabOrder = 0
    object rgTpPesquisa: TRadioGroup
      Left = 1
      Top = 1
      Width = 248
      Height = 63
      Align = alLeft
      Caption = 'Pesquisa por:'
      Columns = 2
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Verdana'
      Font.Style = []
      ItemIndex = 1
      Items.Strings = (
        'C'#243'digo'
        'Descri'#231#227'o'
        'Cod. Barra')
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
    object rg2: TGroupBox
      Left = 249
      Top = 1
      Width = 570
      Height = 63
      Hint = 'Localizar Produto'
      Align = alClient
      Caption = 'Par'#226'metros:'
      Ctl3D = False
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      object btnPesquisar: TSpeedButton
        Left = 525
        Top = 22
        Width = 33
        Height = 33
        Flat = True
        Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FDFEFEEBEEF0E4E4E4F6F3F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFCFCFCF6F6F7F3F8FC876E5B975D37B7886BF2ECE8FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFCDCFD0927F71D66F32F17730A65521D2B09CF7
          F6F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB4A599CA8C69FFA971FF98
          52FA8037A25C30D2B09CF9F9F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCD0C9
          B59076F5AC83FFA46BFF9B52DD7130B1714AD8BEAEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFC8BBB1C59474FAB186FFA569FF9347C86629B98261E7DCD4
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCAA9BCC9D7FFCB58AFFA465FF
          9449BA5C1FC19477EFE7E0FFFFFFF9F9F9F6F6F6F5F5F5F6F6F6F9F9F9FCFCFC
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDBCA4
          92D7A588FFB582FFA260FE883CAF6639CDC6C1D1D2D3C0C0C0B0B0B0A8A8A8B6
          B6B6CACACAD7D7D7E9E9E9FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF6F6F5AE8E78ECB290FFB27EFF9F57C4662D9EA3A67070705353
          536262627575758282828A8A8A9E9E9EC0C0C0D9D9D9F1F1F1FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFB08D73E1A684DA8F63C2BFBD
          6A6C6EA6A6A6EFEFEFFEFEFEFFFFFFFDFDFDF4F4F4E1E1E1B3B3B3ADADADC5C5
          C5F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDE8E4A4
          92859B9895F8FBFDF1F1F1E3E3E3D7D7D7D6D6D6D5D5D5D4D4D4D4D4D4D5D5D5
          F6F6F6E5E5E5B4B4B4D4D4D4FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFAFBFC949697CFD0D1E5E5E5D0D0D0ECECECF2F2F2F9F9F9FCFCFCF8
          F8F8F2F2F2ECECECC4C4C4D5D5D5FCFCFCB5B5B5EDEDEDFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFD1D1D1D1D1D1E8E8E8E6E6E6FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFC6C6C6F6F6F6CECECEFC
          FCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1C1C1E7E7E7E1E1E1FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5
          E5CECECED6D6D6F4F4F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC2C2C2CB
          CBCBD6D6D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFA1A1A1CACACAF2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFEAEAEAD4D4D4B1B1B1B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D5848484C2C2C2D8D8D8FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFEAEAEACFCFCFB4B4B4B2B2B2FCFCFCFEFEFEFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDEBEBEBC8C8C87D7D7DBCBCBCCA
          CACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8B8B8CCCCCCA6A6A6EEEEEE
          FBFBFBFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF6F6F6F1F1F1E7E7E7B9B9
          B97B7B7BBEBEBED1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBCE3
          E3E3BABABABFBFBFE5E5E5FBFBFBFCFCFCFAFAFAFAFAFAF9F9F9F8F8F8FEFEFE
          E9E9E9C0C0C0848484C7C7C7A8A8A8E9E9E9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFE6E6E6C6C6C6ECECECCBCBCBBFBFBFBFBFBFDFDFDFF1F1F1F8F8F8F3
          F3F3E4E4E4C2C2C2A8A8A8A0A0A0BCBCBCDADADAA4A4A4FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCDCDC3C3C3FFFFFFF3F3F3D8D8D8ADAD
          AD9C9C9C9393939494949F9F9FB8B8B8D1D1D1E8E8E8F2F2F2777777F0F0F0FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDBDBDBAEAEAE
          E9E9E9FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F1F1C5C5C58080
          80FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFEEEEEEC4C4C4C3C3C3C5C5C5C8C8C8D0D0D0CBCBCBC0C0C0B9B9B9
          B3B3B3C1C1C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F3E8E8E8E0E0E0D4D4D4D5
          D5D5E2E2E2E6E6E6FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        OnClick = btnPesquisarClick
      end
      object edtLocalizar: TEdit
        Left = 7
        Top = 27
        Width = 513
        Height = 24
        TabOrder = 0
      end
    end
  end
  object pn2: TPanel
    Left = 0
    Top = 65
    Width = 820
    Height = 142
    Align = alClient
    TabOrder = 1
    object GridProduto: TDBGrid
      Left = 1
      Top = 1
      Width = 818
      Height = 140
      Align = alClient
      Ctl3D = False
      DataSource = dmPedidos.dsProduto
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'Verdana'
      TitleFont.Style = []
      OnDblClick = GridProdutoDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'ID'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold]
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COD_BARRA'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold]
          Width = 107
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DECRICAO'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold]
          Width = 320
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UNIDADE'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold]
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SALDO'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold]
          Width = 88
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VLR_COMPRA'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold]
          Width = 102
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VLR_VENDA'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold]
          Width = 102
          Visible = True
        end>
    end
  end
end
