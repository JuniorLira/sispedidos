object frmVendaProdutos: TfrmVendaProdutos
  Left = 485
  Top = 151
  BorderStyle = bsDialog
  Caption = 'Venda de Produtos'
  ClientHeight = 369
  ClientWidth = 591
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  DesignSize = (
    591
    369)
  PixelsPerInch = 96
  TextHeight = 18
  object bvl3: TBevel
    Left = 8
    Top = 70
    Width = 409
    Height = 118
    Style = bsRaised
  end
  object bvl1: TBevel
    Left = 8
    Top = 15
    Width = 409
    Height = 44
    Style = bsRaised
  end
  object lb1: TLabel
    Left = 13
    Top = 7
    Width = 56
    Height = 16
    Caption = 'Cliente:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb4: TLabel
    Left = 14
    Top = 82
    Width = 44
    Height = 14
    Caption = 'Codigo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb5: TLabel
    Left = 111
    Top = 82
    Width = 60
    Height = 14
    Caption = 'Descri'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb6: TLabel
    Left = 14
    Top = 130
    Width = 31
    Height = 14
    Caption = 'Qtde'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb7: TLabel
    Left = 144
    Top = 130
    Width = 71
    Height = 14
    Caption = 'Vlr. Unit'#225'rio'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb8: TLabel
    Left = 278
    Top = 130
    Width = 52
    Height = 14
    Caption = 'Vlr. Total'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb3: TLabel
    Left = 13
    Top = 62
    Width = 62
    Height = 16
    Caption = 'Produto:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object shp1: TShape
    Left = 438
    Top = 322
    Width = 148
    Height = 41
    Brush.Color = clMoneyGreen
    Shape = stRoundRect
  end
  object lb9: TLabel
    Left = 440
    Top = 305
    Width = 61
    Height = 16
    Caption = 'Subtotal'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edtCliente: TEdit
    Left = 14
    Top = 25
    Width = 92
    Height = 24
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    OnExit = edtClienteExit
  end
  object dblCliente: TDBLookupComboBox
    Left = 109
    Top = 25
    Width = 301
    Height = 24
    Ctl3D = False
    KeyField = 'ID'
    ListField = 'NM_FANTASIA'
    ListSource = dmPedidos.dsCliente
    ParentCtl3D = False
    TabOrder = 1
    OnExit = dblClienteExit
  end
  object edtCodProd: TEdit
    Left = 13
    Top = 98
    Width = 92
    Height = 24
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
    OnExit = edtCodProdExit
    OnKeyPress = editsKeyPress
  end
  object edtDescricao: TEdit
    Left = 110
    Top = 98
    Width = 300
    Height = 24
    TabStop = False
    Color = clMoneyGreen
    Ctl3D = False
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 3
  end
  object edtQtde: TEdit
    Left = 15
    Top = 146
    Width = 125
    Height = 24
    Anchors = [akTop]
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 4
    Text = '1,00'
    OnExit = edtQtdeExit
    OnKeyPress = editsKeyPress
  end
  object edtVlrUnit: TEdit
    Left = 145
    Top = 146
    Width = 130
    Height = 24
    Anchors = [akTop]
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 5
    Text = '0,00'
    OnExit = edtVlrUnitExit
    OnKeyPress = editsKeyPress
  end
  object edtVlrTotal: TEdit
    Left = 280
    Top = 146
    Width = 130
    Height = 24
    Anchors = [akTop]
    Ctl3D = False
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 6
    Text = '0,00'
  end
  object GridItens: TDBGrid
    Left = 8
    Top = 202
    Width = 427
    Height = 161
    Ctl3D = False
    DataSource = dsItens
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 7
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -16
    TitleFont.Name = 'Verdana'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CODIGO'
        Title.Caption = 'C'#243'digo'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Verdana'
        Title.Font.Style = []
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Produto'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Verdana'
        Title.Font.Style = []
        Width = 170
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTDE'
        Title.Caption = 'Qtde'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Verdana'
        Title.Font.Style = []
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VLRUNIT'
        Title.Caption = 'Vlr. Unit'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Verdana'
        Title.Font.Style = []
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VLRTOTAL'
        Title.Caption = 'Vlr. Total'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Verdana'
        Title.Font.Style = []
        Width = 62
        Visible = True
      end>
  end
  object btnInserir: TBitBtn
    Left = 422
    Top = 15
    Width = 166
    Height = 25
    Caption = '[F1] Inserir Item'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = btnInserirClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF08
      780E08780EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF08780E76F9A70DA31B08780EFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF08780E76
      F9A70EAA1D08780EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF08780E76F9A70EA81C08780EFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF08780E76
      F9A710AA1F08780EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      08780E08780E08780E08780E08780E76F9A719B02C08780E08780E08780E0878
      0E08780EFF00FFFF00FFFF00FF08780E76F9A755E38349DA7242D36837C8562A
      B94322B3371CB23016AF270FA81D0EA91B0DA21B08780EFF00FFFF00FF08780E
      76F9A776F9A776F9A776F9A776F9A776F9A72CBB4876F9A776F9A776F9A776F9
      A776F9A708780EFF00FFFF00FFFF00FF08780E08780E08780E08780E08780E76
      F9A73CCB5D08780E08780E08780E08780E08780EFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF08780E76F9A749D97208780EFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF08780E76
      F9A755E28208780EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF08780E76F9A763F09708780EFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF08780E76
      F9A776F9A708780EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FF08780E08780EFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
  end
  object btnApagar: TBitBtn
    Left = 422
    Top = 41
    Width = 166
    Height = 25
    Caption = '[F2] Apagar Item'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    OnClick = btnApagarClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FF0104A20104A20104A20104A20104A20104A20104A20104A20104A20104
      A2FF00FFFF00FFFF00FFFF00FFFF00FF0104A25983FF0026FF0030FF0030FB00
      2FF2002FE9002EE10030D80031D00034CB0104A2FF00FFFF00FFFF00FFFF00FF
      0104A2ABC2FF6480FF6688FF6688FF6687FA6787F56787F05779E94D70E44D74
      E20104A2FF00FFFF00FFFF00FFFF00FFFF00FF0104A20104A20104A20104A201
      04A20104A20104A20104A20104A20104A2FF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
  end
  object btnCancelar: TBitBtn
    Left = 422
    Top = 68
    Width = 166
    Height = 25
    Caption = '[F3] Cancelar Pedido'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    OnClick = btnCancelarClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FF00009A00009AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00009A0000
      9AFF00FFFF00FFFF00FFFF00FFFF00FF00009A174AFD103BF400009AFF00FFFF
      00FFFF00FFFF00FF00009A002CF80030FC00009AFF00FFFF00FFFF00FFFF00FF
      00009A1A47F81A4CFF123BF100009AFF00FFFF00FF00009A012DF60132FF002A
      F300009AFF00FFFF00FFFF00FFFF00FFFF00FF00009A1C47F61B4DFF143EF400
      009A00009A002DF80134FF032BF200009AFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FF00009A1D48F61D50FF103DFB0431FE0132FF002CF600009AFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00009A1A48F913
      42FF0C3CFF0733F600009AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FF00009A214EFC1D4BFF1847FF1743F600009AFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00009A2E5BF92C5FFF22
      4DF8204BF82355FF1B46F600009AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FF00009A3664FA386BFF2D59F400009A00009A224CF42558FF1D49F60000
      9AFF00FFFF00FFFF00FFFF00FFFF00FF00009A4071FA4274FF325DF100009AFF
      00FFFF00FF00009A224DF1275AFF204CF800009AFF00FFFF00FFFF00FFFF00FF
      00009A497AFC3B66F300009AFF00FFFF00FFFF00FFFF00FF00009A2550F42655
      FA00009AFF00FFFF00FFFF00FFFF00FFFF00FF00009A00009AFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FF00009A00009AFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
  end
  object btnConcluir: TBitBtn
    Left = 422
    Top = 94
    Width = 166
    Height = 25
    Caption = '[F4] Concluir Pedido'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    OnClick = btnConcluirClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FF4A4A4A4A4A4A4A4A4A4A4A4AFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4A4A4A8D8587938F8F8D
      8E8E9A9A9A4A4A4A4A4A4A4A4A4AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FF4A4A4A8B888B8480839592934141415252525D5D5D3F3F3F8282824A4A
      4A4A4A4A4A4A4AFF00FFFF00FFFF00FF4A4A4A8986898D8A8D87848795929486
      87876767673434340000000000001515158E8B8D908B905D5C5CFF00FF4A4A4A
      D8D6D9D5D1D5AEA9AD838083EBE9EBE1E2E2BEBEBEA4A4A46D6D6D3939393635
      365763575671565D5C5C4A4A4AA8A7A9E0DFE2E0DFE1F5F7F8989A9C878687B7
      B6B7E1E1E1EEF0F0EAEFEFDEE2E2BFBEC0899B89529A535D5C5C4A4A4AF3F2F5
      EBEBEEF9F8F8DABEBED1B9BBA8ABAD8485866C6D6E877D7EBA9B9CD1BFC0DDDB
      DCECEAEBE1E1E15D5C5C4A4A4AFDFDFDFEFFFFD6ADAAC45251A95A5B9A7C7DCD
      BEBDDBDBDCC59F9F952322B056578F87888882837C7B7BFF00FF4A4A4AFFFFFF
      FFFFFFB34F50CB5D5DB67070974240BE8685FFFFFFDCB7B7942626B34F504644
      44464444FF00FFFF00FF5959594A4A4AD6D9DAB34F50CC5D5CBD6E6EAF6F6EA0
      6E6FD6D2D0D8A8A89A2A2AB34F50FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      4A4A4AB34F50C85F5DC66A6ACF8382CB7D7DC87474CC7978CD7373B34F50FF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB34F50C57D7AEEDFDEF2E6E5F3
      E5E4F2E3E2F5ECEBD19492B34F50FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFB34F50D19693F6FCFAD2D0CFD2D0CFD2D0CFEDF0F0D19492B34F50FF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB34F50CF9391F6F6F6E5E1E0E5
      E1E0E5E1E0EAECEBD19492B34F50FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFB34F50D19492F6F6F6D2D0CFD2D0CFD2D0CFEAECEBD19492B34F50FF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC28A88D8D3D3D8D3D3D8
      D3D3D8D3D3D8D3D3C28A88FF00FFFF00FFFF00FFFF00FFFF00FF}
  end
  object btnClear: TBitBtn
    Left = 422
    Top = 174
    Width = 166
    Height = 25
    Caption = '[ESC] Limpar Editores'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 14
    OnClick = btnClearClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FF1877EA1574E0FF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF1776E615
      73E7136FDC126ED30F6AC9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FF1676CA268BE5278CEB2184E3116CCF0C67C40B64BB0962
      B4075CA9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF258BD83EABEE45
      B4F72687DD3DAAF41E80D50F69BF065BAA0458A004579A035495FF00FFFF00FF
      FF00FFFF00FF01500A319CF739A4F41B6DC448B7FA2687DD41ADFA3BA7F81F81
      D4197ACC065B9F025291FF00FFFF00FFFF00FF0B6344185F6901500A166AD425
      81CE1160C02889E340ACFE359EF23CA6FF3CA6FF2D95EFFF00FFFF00FFFF00FF
      06630925AC461CB62F1CB62F1CB62F01500A01500A196DA32380E12C8FEB38A3
      FF38A1FFFF00FFFF00FFFF00FF096D111C9A3147F77936E5541CB62F0E912D12
      805B1CB62F1CB62F01500A01500AFF00FFFF00FFFF00FFFF00FFFF00FF1C982F
      18902935E05D21C2390CA015008600008300068E0F1DA6572D9197FF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FF035A0617992811AA1D018202007B0004
      8A0B005D01005D01FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      1387232AD149048807005C00005D01046D0AFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FF0E78182CD44D068C09005400FF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF08690E2BCE49
      07960D005800FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FF0D82170C9D15005E00FF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0B8913006501
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    Spacing = 1
  end
  object edtSubTotal: TEdit
    Left = 443
    Top = 327
    Width = 139
    Height = 31
    BorderStyle = bsNone
    CharCase = ecUpperCase
    Color = clMoneyGreen
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Verdana'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 15
    Text = '0,00'
  end
  object btnCalculadora: TBitBtn
    Left = 422
    Top = 147
    Width = 166
    Height = 25
    Caption = '[F8] Calculadora'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 13
    OnClick = btnCalculadoraClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF994517
      9945179945179945179945179945179945179945179945179945179945179945
      17994517994517FF00FFCF8923FFC47DE3A45CDC994CFFAB45ED9935D18427FB
      951DFF9311FA8809FA8201FA8000FA8000FB8100FF8800994517E0970EE9AF7A
      3E352C393129C686416C4F2D2B2723A76922C9771B362B1C3A2D1B382B1C3B2C
      18473116F37F00994517E0970EFFC289BB8B5BAF7F4DFAAB52CA883EA66E2EE7
      8F28E28A2A9A653CB56B22D77407A35E21A66021FE8600994517E0970EE7B181
      453B3341372EC58B4D725535352D25A16A287F63791A2AD452498ABB702F1C2E
      C62030BDE57A0A994517E0970EFEC48BAA835D9F7851FCB465C68A4A8A6034EA
      9534E2903D6E587BB7743BFE8C0A84594E8A5944FE8600994517E0970EE2AD7F
      483E34403730BF8F5970563B2E29259F6C3086697F1427DA534B8FC077321C2F
      C51F2FBDDA780F994517E0970EFFC78FFBB77BF2B077FFC179FEAF63EA9C4FFF
      AB46FFA435DA8E41FB9625FF9713E98318EE800DFF8800994517E0970EF2BB89
      D8D4AFD9D7B0D4D0AAD8D1A6DACF9DD4C992DEC688FFAD42FB9625EF8C1EF388
      10F68608FF8801994517E0970EC79A7350C6BB51CBC051CBC051CBC151CBC14B
      CBC474D7CEFBB560BB7426A16723985F1CB06813FF8B04994517EAA127F6BA8B
      C4976CC4986CC4986CC4976BC49560C18E53D19650FFAB4FF39A38FB9A2DE086
      21EF8916FF910A994517FF00FFDC9103E79F23EBA534E9A334E29E34EEA747EE
      A340E99C35E3932DE28E23E0881CE18315DE7F0FC56B0CFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
  end
  object btnBuscarProduto: TBitBtn
    Left = 422
    Top = 121
    Width = 166
    Height = 25
    Caption = '[F7] Buscar Produto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    OnClick = btnBuscarProdutoClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FF0975A7075C840C6F9DFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0670A43F
      9AB86E533B15212618769FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FF0670A431CDFF50A8BFFFDAA07C5F451B3B4CFF00FF0052
      86005286FF00FFFF00FFFF00FFFF00FFFF00FF0670A42FBFF028ACDF59D2FE78
      C9E7E3BB8FD7A8792832391D7BAB0052860052860A587EFF00FFFF00FFFF00FF
      0670A443D0FE6CDEFF5CB4DBAEDDF3F1FFFFDEDBD59B88732C5F765FBDE665BF
      E083756117161504557F0C7BAE2998C877DDFFA3E5FFCFF1FFDBF0F95AA9C8B1
      D8E98BCAE5379AC61AABDA4AD0FF2EB8E7D0BE9D7C5639023B57086C9F0670A4
      CDEBF9FAFFFFF2FCFFDBF5FF60BDE00697C6008EBC0089B80BA9D534C7FA33CF
      FF7EB8BF8B6B4C0142620670A4BCDBEA0670A4A1D2E693D9F772DBFF53D6FF33
      C0F1189CCE058CBC00A0CB1FB5E437CBFC23B1E51A6687025F8FFF00FF0670A4
      0670A40670A426A1D226ADE031C2F537C9FB34C4F72AB5E7128DBE0172A10F7B
      AFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0E85B80C7EB213
      93C50B8DC0006C9FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
  end
  object pnConclusaoPedido: TPanel
    Left = 127
    Top = 59
    Width = 305
    Height = 246
    BevelInner = bvLowered
    TabOrder = 16
    Visible = False
    DesignSize = (
      305
      246)
    object lb11: TLabel
      Left = 10
      Top = 78
      Width = 94
      Height = 14
      Caption = 'Desconto (%)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb12: TLabel
      Left = 157
      Top = 78
      Width = 83
      Height = 14
      Caption = 'Desconto R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb13: TLabel
      Left = 10
      Top = 169
      Width = 134
      Height = 14
      Caption = 'Vlr. Venda (Liquido)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb2: TLabel
      Left = 10
      Top = 125
      Width = 99
      Height = 14
      Caption = 'Valor Recebido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb10: TLabel
      Left = 157
      Top = 125
      Width = 76
      Height = 14
      Caption = 'Valor Troco'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb14: TLabel
      Left = 157
      Top = 169
      Width = 122
      Height = 14
      Caption = 'Vlr. Venda (Bruto)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object pn1: TPanel
      Left = 2
      Top = 2
      Width = 301
      Height = 36
      Align = alTop
      BevelInner = bvLowered
      BevelWidth = 2
      BorderStyle = bsSingle
      Caption = 'Finaliza'#231#227'o da Venda'
      Color = clActiveBorder
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
    object btnConfirmar: TBitBtn
      Left = 63
      Top = 216
      Width = 116
      Height = 26
      Caption = '&Confirmar'
      TabOrder = 6
      OnClick = btnConfirmarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object btnCancel: TBitBtn
      Left = 184
      Top = 216
      Width = 116
      Height = 26
      Cancel = True
      Caption = 'Ca&ncelar'
      TabOrder = 8
      OnClick = btnCancelClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object edtDescPerc: TEdit
      Left = 10
      Top = 95
      Width = 138
      Height = 24
      Anchors = [akTop]
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      Text = '0,00'
      OnExit = edtDescontosExit
      OnKeyPress = editsKeyPress
    end
    object edtDescReal: TEdit
      Left = 157
      Top = 95
      Width = 138
      Height = 24
      Anchors = [akTop]
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 2
      Text = '0,00'
      OnExit = edtDescontosExit
      OnKeyPress = editsKeyPress
    end
    object edtVlrTotVenda: TEdit
      Left = 10
      Top = 185
      Width = 138
      Height = 24
      TabStop = False
      Anchors = [akTop]
      Color = clMoneyGreen
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 5
      Text = '0,00'
      OnExit = edtQtdeExit
      OnKeyPress = editsKeyPress
    end
    object rgTpVenda: TRadioGroup
      Left = 2
      Top = 38
      Width = 301
      Height = 37
      Align = alTop
      Caption = 'Tipo de Venda:'
      Columns = 2
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'A Vista'
        'A Prazo')
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
    object edtVlrRecebido: TEdit
      Left = 10
      Top = 141
      Width = 138
      Height = 24
      Anchors = [akTop]
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 3
      Text = '0,00'
      OnExit = edtVlrRecebidoExit
      OnKeyPress = editsKeyPress
    end
    object edtVlrTroco: TEdit
      Left = 157
      Top = 141
      Width = 138
      Height = 24
      TabStop = False
      Anchors = [akTop]
      Color = cl3DLight
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 4
      Text = '0,00'
      OnKeyPress = editsKeyPress
    end
    object edtVlrBrutoVenda: TEdit
      Left = 157
      Top = 185
      Width = 138
      Height = 24
      TabStop = False
      Anchors = [akTop]
      Color = clMoneyGreen
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 9
      Text = '0,00'
      OnExit = edtQtdeExit
      OnKeyPress = editsKeyPress
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnShortCut = ApplicationEvents1ShortCut
    Left = 384
    Top = 280
  end
  object dsItens: TDataSource
    DataSet = cdsItens
    Left = 304
    Top = 280
  end
  object cdsItens: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 336
    Top = 280
    Data = {
      9D0000009619E0BD0100000018000000050000000000030000009D0006434F44
      49474F04000100000000000944455343524943414F0100490000000100055749
      4454480200020032000451544445080004000000000007564C52554E49540800
      04000000010007535542545950450200490006004D6F6E65790008564C52544F
      54414C080004000000010007535542545950450200490006004D6F6E65790000
      00}
    object cdsItensCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object cdsItensDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 50
    end
    object cdsItensQTDE: TFloatField
      FieldName = 'QTDE'
    end
    object cdsItensVLRUNIT: TCurrencyField
      FieldName = 'VLRUNIT'
    end
    object cdsItensVLRTOTAL: TCurrencyField
      FieldName = 'VLRTOTAL'
    end
  end
  object frxRelVenda: TfrxReport
    Version = '6.0.10'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43351.489331111100000000
    ReportOptions.LastChange = 43351.514724849500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 464
    Top = 240
    Datasets = <
      item
        DataSet = frxDBVendaCab
        DataSetName = 'frxDBVendaCab'
      end
      item
        DataSet = frxDBVendaItens
        DataSetName = 'frxDBVendaItens'
      end>
    Variables = <
      item
        Name = ' Parametros'
        Value = Null
      end
      item
        Name = 'CLIENTE'
        Value = ''
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Align = baCenter
          Left = 115.275665000000000000
          Width = 487.559370000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Cupom de Venda de Produtos')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 22.897650000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 75.590600000000000000
        ParentFont = False
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          Left = 41.338590000000000000
          Top = 0.779530000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8 = (
            'Venda N'#194#186' ')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 112.590600000000000000
          Top = 0.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%g'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[frxDBVendaCab."ID"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 229.756030000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8 = (
            'Data e Hora:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 309.126160000000000000
          Width = 366.614410000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[<frxDBVendaCab."DT_VENDA">] - [<frxDBVendaCab."HR_VENDA">]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 42.338590000000000000
          Top = 22.456710000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8 = (
            'Tipo de Venda:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 139.842610000000000000
          Top = 22.677180000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8 = (
            '[IIF(<frxDBVendaCab."TP_VENDA"> = '#39'V'#39','#39'A Vista'#39', '#39'A Prazo'#39')]')
        end
        object Memo10: TfrxMemoView
          Left = 238.110390000000000000
          Top = 22.677180000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8 = (
            'Cliente:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 302.362400000000000000
          Top = 22.677180000000000000
          Width = 374.173470000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8 = (
            '[<frxDBVendaCab."ID_CLIENTE">] - [CLIENTE]')
        end
        object Line3: TfrxLineView
          Top = 45.354360000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo12: TfrxMemoView
          Left = 3.779530000000000000
          Top = 49.133890000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Codigo')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 67.913420000000000000
          Top = 49.133890000000000000
          Width = 332.598640000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Descri'#195#167#195#163'o')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 404.275820000000000000
          Top = 49.133890000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Quantidade')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 508.425480000000000000
          Top = 49.133890000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Valor Unit'#195#161'rio')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 613.913730000000000000
          Top = 49.133890000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Valor Total')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Top = 71.811070000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 24.566929133858270000
        Top = 204.094620000000000000
        Width = 718.110700000000000000
        DataSet = frxDBVendaItens
        DataSetName = 'frxDBVendaItens'
        RowCount = 0
        object frxDBVendaItensCODIGO: TfrxMemoView
          IndexTag = 1
          Left = 3.779530000000000000
          Top = 1.889763780000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataSet = frxDBVendaItens
          DataSetName = 'frxDBVendaItens'
          DisplayFormat.FormatStr = '%g'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBVendaItens."CODIGO"]')
          ParentFont = False
        end
        object frxDBVendaItensDESCRICAO: TfrxMemoView
          IndexTag = 1
          Left = 68.031540000000000000
          Top = 1.889763780000000000
          Width = 332.598640000000000000
          Height = 18.897650000000000000
          DataField = 'DESCRICAO'
          DataSet = frxDBVendaItens
          DataSetName = 'frxDBVendaItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[frxDBVendaItens."DESCRICAO"]')
          ParentFont = False
        end
        object frxDBVendaItensQTDE: TfrxMemoView
          IndexTag = 1
          Left = 404.409710000000000000
          Top = 1.889763780000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          DataSet = frxDBVendaItens
          DataSetName = 'frxDBVendaItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBVendaItens."QTDE"]')
          ParentFont = False
        end
        object frxDBVendaItensVLRUNIT: TfrxMemoView
          IndexTag = 1
          Left = 508.346456690000000000
          Top = 1.889763780000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          DataSet = frxDBVendaItens
          DataSetName = 'frxDBVendaItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBVendaItens."VLRUNIT"]')
          ParentFont = False
        end
        object frxDBVendaItensVLRTOTAL: TfrxMemoView
          IndexTag = 1
          Left = 613.795275590000000000
          Top = 1.889763780000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          DataSet = frxDBVendaItens
          DataSetName = 'frxDBVendaItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBVendaItens."VLRTOTAL"]')
          ParentFont = False
        end
        object Line5: TfrxLineView
          Left = 3.779530000000000000
          Top = 22.677180000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 381.732530000000000000
        Width = 718.110700000000000000
        object Memo4: TfrxMemoView
          Left = 5.118120000000000000
          Top = 11.724409450000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            'Desenvolvido por: J'#195#186'nior Lira')
          ParentFont = False
        end
        object SysMemo1: TfrxSysMemoView
          Left = 540.000310000000000000
          Top = 11.724409450000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[DATE]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 419.055350000000000000
          Top = 11.724409450000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            'P'#195#161'gina Impressa Em:')
          ParentFont = False
        end
        object SysMemo2: TfrxSysMemoView
          Left = 642.047620000000000000
          Top = 11.724409450000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[TIME]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Left = 3.779530000000000000
          Top = 9.118120000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Frame.Typ = []
        Height = 68.031540000000000000
        Top = 291.023810000000000000
        Width = 718.110700000000000000
        object Memo17: TfrxMemoView
          Left = 497.220780000000000000
          Top = 1.779530000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Valor Bruto R$:')
          ParentFont = False
        end
        object frxDBVendaCabVLR_TOT_VENDA: TfrxMemoView
          IndexTag = 1
          Left = 612.268090000000000000
          Top = 2.000000000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          DataSet = frxDBVendaCab
          DataSetName = 'frxDBVendaCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Frame.Typ = []
          Memo.UTF8 = (
            '[frxDBVendaCab."VLR_TOT_VENDA"]')
        end
        object Memo18: TfrxMemoView
          Left = 497.323130000000000000
          Top = 22.677180000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Desconto R$:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 497.323130000000000000
          Top = 44.354360000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Valor Liquido R$:')
          ParentFont = False
        end
        object frxDBVendaCabVLR_DESC_REAL: TfrxMemoView
          IndexTag = 1
          Left = 611.709030000000000000
          Top = 22.677165350000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          DataSet = frxDBVendaCab
          DataSetName = 'frxDBVendaCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[frxDBVendaCab."VLR_DESC_REAL"]')
          ParentFont = False
        end
        object frxDBVendaCabVLR_LIQ_VENDA: TfrxMemoView
          IndexTag = 1
          Left = 611.709030000000000000
          Top = 44.354360000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          DataSet = frxDBVendaCab
          DataSetName = 'frxDBVendaCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[frxDBVendaCab."VLR_LIQ_VENDA"]')
          ParentFont = False
        end
        object COUNT: TfrxMemoView
          IndexTag = 1
          Left = 119.724490000000000000
          Top = 1.889763780000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[COUNT(MasterData1)]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 11.338590000000000000
          Top = 1.889763779527559000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            'N'#194#186' de Volumes:')
          ParentFont = False
        end
      end
    end
  end
  object frxDBVendaCab: TfrxDBDataset
    UserName = 'frxDBVendaCab'
    CloseDataSource = False
    DataSet = dmPedidos.qryVendaCab
    BCDToCurrency = False
    Left = 504
    Top = 240
  end
  object frxDBVendaItens: TfrxDBDataset
    UserName = 'frxDBVendaItens'
    CloseDataSource = False
    DataSet = cdsItens
    BCDToCurrency = False
    Left = 544
    Top = 240
  end
end
