unit unRelVendas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, DBCtrls, frxClass,
  frxDBSet, Clipbrd;

type
  TfrmRelVendas = class(TForm)
    lb3: TLabel;
    dblCliente: TDBLookupComboBox;
    rg2: TGroupBox;
    lb1: TLabel;
    lb2: TLabel;
    dtpDtInicial: TDateTimePicker;
    dtpDtFinal: TDateTimePicker;
    cbFiltroDatas: TCheckBox;
    rgTipoVenda: TRadioGroup;
    bvl1: TBevel;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    frxRelVenda: TfrxReport;
    frxDBVenda: TfrxDBDataset;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cbFiltroDatasClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelVendas: TfrmRelVendas;

implementation

{$R *.dfm}

uses unDmPedidos, IBCustomDataSet;

procedure TfrmRelVendas.FormClose(Sender: TObject;
  var Action: TCloseAction);
 begin
  dmPedidos.qryCliente.Close;
 end;

procedure TfrmRelVendas.FormShow(Sender: TObject);
 begin
  dmPedidos.qryCliente.Open;
 end;

procedure TfrmRelVendas.cbFiltroDatasClick(Sender: TObject);
 begin
  if cbFiltroDatas.Checked then
   begin
    dtpDtInicial.Color   := clWindow;
    dtpDtFinal.Color     := clWindow;
    dtpDtInicial.Enabled := True;
    dtpDtFinal.Enabled   := True;
   end
  else
   begin
    dtpDtInicial.Color   := clBtnFace;
    dtpDtFinal.Color     := clBtnFace;
    dtpDtInicial.Enabled := False;
    dtpDtFinal.Enabled   := False;
   end;
 end;

procedure TfrmRelVendas.btnConfirmarClick(Sender: TObject);
 begin
  with dmPedidos do
   begin
    qryVendaCab.Close;
    qryVendaCab.SelectSQL.Clear;
    qryVendaCab.SelectSQL.Add('Select * From VENDA_CAB ');
    if (dblCliente.KeyValue <> null) then
     begin
      qryVendaCab.SelectSQL.Add('WHERE ID_CLIENTE = ' + QuotedStr(dblCliente.KeyValue));
      if (cbFiltroDatas.Checked) then
       qryVendaCab.SelectSQL.Add(' AND DT_VENDA >= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtInicial.DateTime)) +
                                ' AND DT_VENDA <= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtFinal.DateTime)));
      if (rgTipoVenda.ItemIndex <> 2) then
       begin
        if (rgTipoVenda.ItemIndex = 0) then
         qryVendaCab.SelectSQL.Add(' AND TP_VENDA = ' + QuotedStr('V'))
        else
         qryVendaCab.SelectSQL.Add(' AND TP_VENDA = ' + QuotedStr('P'))
       end;
     end
    else
    if (cbFiltroDatas.Checked) then
     begin
      qryVendaCab.SelectSQL.Add(' WHERE DT_VENDA >= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtInicial.DateTime)) +
                                ' AND DT_VENDA <= ' + QuotedStr(FormatDateTime('dd.mm.yyyy',dtpDtFinal.DateTime)));
      if (rgTipoVenda.ItemIndex <> 2) then
       begin
        if (rgTipoVenda.ItemIndex = 0) then
         qryVendaCab.SelectSQL.Add(' AND TP_VENDA = ' + QuotedStr('V'))
        else
         qryVendaCab.SelectSQL.Add(' AND TP_VENDA = ' + QuotedStr('P'))
       end;
     end
    else if (rgTipoVenda.ItemIndex <> 2) then
     begin
      if (rgTipoVenda.ItemIndex = 0) then
       qryVendaCab.SelectSQL.Add(' WHERE TP_VENDA = ' + QuotedStr('V'))
      else
       qryVendaCab.SelectSQL.Add(' WHERE TP_VENDA = ' + QuotedStr('P'))
     end;
    qryVendaCab.Open;
    frxRelVenda.PrepareReport(True);
    frxRelVenda.ShowPreparedReport;
   end;
 end;

end.
