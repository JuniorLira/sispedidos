object frmConexao: TfrmConexao
  Left = 495
  Top = 298
  BorderStyle = bsDialog
  Caption = 'Local da Conex'#227'o'
  ClientHeight = 105
  ClientWidth = 442
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 18
  object lb1: TLabel
    Left = 6
    Top = 3
    Width = 199
    Height = 18
    Caption = 'Caminho do Banco de Dados.:'
  end
  object bvl1: TBevel
    Left = 5
    Top = 23
    Width = 430
    Height = 47
    Shape = bsFrame
    Style = bsRaised
  end
  object btnLocal: TButton
    Left = 371
    Top = 33
    Width = 59
    Height = 25
    Caption = '...'
    TabOrder = 0
    OnClick = btnLocalClick
  end
  object edtFile: TEdit
    Left = 11
    Top = 33
    Width = 354
    Height = 24
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
  end
  object btnConfirmar: TBitBtn
    Left = 323
    Top = 75
    Width = 113
    Height = 25
    Caption = '&Confirmar'
    TabOrder = 2
    OnClick = btnConfirmarClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object dlgBase: TOpenDialog
    Filter = 'Arquivos Firebird|*.FDB'
    InitialDir = 'C:\'
    Left = 264
    Top = 32
  end
end
