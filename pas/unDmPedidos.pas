unit unDmPedidos;

interface

uses
  SysUtils, Classes, IBDatabase, DB, IBCustomDataSet, IBQuery, IniFiles, Forms, Windows;

type
  TdmPedidos = class(TDataModule)
    Banco: TIBDatabase;
    TBanco: TIBTransaction;
    dsCliente: TDataSource;
    dsProduto: TDataSource;
    dsMarca: TDataSource;
    qryCliente: TIBDataSet;
    qryClienteID: TIntegerField;
    qryClienteTP_PESSOA: TIBStringField;
    qryClienteRZ_SOCIAL: TIBStringField;
    qryClienteNM_FANTASIA: TIBStringField;
    qryClienteDOCUMENTO: TIBStringField;
    qryClienteENDERECO: TIBStringField;
    qryClienteNUMERO: TIBStringField;
    qryClienteBAIRRO: TIBStringField;
    qryClienteCEP: TIBStringField;
    qryClienteCIDADE: TIBStringField;
    qryClienteUF: TIBStringField;
    qryClienteTELEFONE: TIBStringField;
    qryClienteCELULAR: TIBStringField;
    qryClienteEMAIL: TIBStringField;
    qryClienteDT_CADASTRO: TDateField;
    qryClienteDT_ALTERACAO: TDateField;
    qryProduto: TIBDataSet;
    qryProdutoID: TIntegerField;
    qryProdutoDECRICAO: TIBStringField;
    qryProdutoCOD_BARRA: TIBStringField;
    qryProdutoUNIDADE: TIBStringField;
    qryProdutoID_MARCA: TIntegerField;
    qryProdutoVLR_COMPRA: TIBBCDField;
    qryProdutoVLR_VENDA: TIBBCDField;
    qryProdutoQTD_MINIMA: TIBBCDField;
    qryProdutoSALDO: TIBBCDField;
    qryProdutoDT_CADASTRO: TDateField;
    qryProdutoDT_ALTERACAO: TDateField;
    qryMarca: TIBDataSet;
    qryMarcaID: TIntegerField;
    qryMarcaNOME: TIBStringField;
    qryMarcaDT_CADASTRO: TDateField;
    qryMarcaDT_ALTERACAO: TDateField;
    qryVendaCab: TIBDataSet;
    qryVendaItens: TIBDataSet;
    dsVendaCab: TDataSource;
    dsVendaItens: TDataSource;
    qryVendaCabID: TIntegerField;
    qryVendaCabID_CLIENTE: TIntegerField;
    qryVendaCabTP_VENDA: TIBStringField;
    qryVendaCabVLR_TOT_VENDA: TIBBCDField;
    qryVendaCabVLR_LIQ_VENDA: TIBBCDField;
    qryVendaCabVLR_DESC_REAL: TIBBCDField;
    qryVendaCabVLR_DESC_PERC: TIBBCDField;
    qryVendaCabDT_VENDA: TDateField;
    qryVendaCabHR_VENDA: TTimeField;
    qryVendaItensID_VENDA: TIntegerField;
    qryVendaItensID_PRODUTO: TIntegerField;
    qryVendaItensQTDE: TIBBCDField;
    qryVendaItensVLR_ITEM: TIBBCDField;
    qryVendaItensVLT_TOT_ITEM: TIBBCDField;
        
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmPedidos: TdmPedidos;

implementation

{$R *.dfm}

end.
