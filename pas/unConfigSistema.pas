unit unConfigSistema;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls, IniFiles;

type
  TfrmConfigSistema = class(TForm)
    pn1: TPanel;
    pn2: TPanel;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    pn3: TPanel;
    pgcConfig: TPageControl;
    tabEstoque: TTabSheet;
    tabImpressao: TTabSheet;
    tabBancoDados: TTabSheet;
    cbContEstoque: TCheckBox;
    cbPermVenSemSaldo: TCheckBox;
    cbImpresDireta: TCheckBox;
    lb1: TLabel;
    edtCaminhoImpressora: TEdit;
    dlgBase: TOpenDialog;
    edtCaminhoBanco: TEdit;
    btnLocal: TButton;
    lb2: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnLocalClick(Sender: TObject);
    procedure cbImpresDiretaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfigSistema: TfrmConfigSistema;

implementation

{$R *.dfm}

uses unDmPedidos, DB;

procedure TfrmConfigSistema.FormActivate(Sender: TObject);
 begin
  pgcConfig.ActivePage := tabEstoque;
 end;

procedure TfrmConfigSistema.FormCreate(Sender: TObject);
var
  IniFile : TIniFile;
 begin
  IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini');
  try
   if (IniFile.ReadString('ESTOQUE', 'Controla Estoque', '') = 'S') then
    cbContEstoque.Checked := True
   else
    cbContEstoque.Checked := False;
   if (IniFile.ReadString('ESTOQUE', 'Vende Sem Saldo', '') = 'S') then
    cbPermVenSemSaldo.Checked := True
   else
    cbPermVenSemSaldo.Checked := False;
   if (IniFile.ReadString('IMPRESSAO', 'Impressao Direta', '') = 'S') then
    cbImpresDireta.Checked := True
   else
    cbImpresDireta.Checked := False;
   edtCaminhoImpressora.Text := IniFile.ReadString('IMPRESSAO', 'Local Impressora', '');
   edtCaminhoBanco.Text := IniFile.ReadString('BANCO', 'Caminho', '');
  finally
   IniFile.Free;
  end;

  if not(cbImpresDireta.Checked) then
   begin
    edtCaminhoImpressora.Enabled := False;
    edtCaminhoImpressora.Color := clBtnFace;
   end;
 end;

procedure TfrmConfigSistema.btnConfirmarClick(Sender: TObject);
var
  ContEst, VendSemSaldo, ImpDireta : String;
  IniFile : TIniFile;
 begin
  if (cbContEstoque.Checked) then
   ContEst := 'S'
  else
   ContEst := 'N';
  if (cbPermVenSemSaldo.Checked) then
   VendSemSaldo := 'S'
  else
   VendSemSaldo := 'N';
  if (cbImpresDireta.Checked) then
   ImpDireta := 'S'
  else
   ImpDireta := 'N';
  IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini');
  try
   IniFile.WriteString('ESTOQUE', 'Controla Estoque', ContEst);
   IniFile.WriteString('ESTOQUE', 'Vende Sem Saldo', VendSemSaldo);
   IniFile.WriteString('IMPRESSAO', 'Impressao Direta', ImpDireta);
   IniFile.WriteString('IMPRESSAO', 'Local Impressora', edtCaminhoImpressora.Text);
   IniFile.WriteString('BANCO', 'Caminho', edtCaminhoBanco.Text);
  finally
   IniFile.Free;
  end;
 end;

procedure TfrmConfigSistema.btnLocalClick(Sender: TObject);
 begin
  dlgBase.Execute();
  edtCaminhoBanco.Text := dlgBase.FileName;
 end;

procedure TfrmConfigSistema.cbImpresDiretaClick(Sender: TObject);
 begin
  if cbImpresDireta.Checked then
   begin
    edtCaminhoImpressora.Enabled := True;
    edtCaminhoImpressora.Color := clWindow;
   end
  else
   begin
    edtCaminhoImpressora.Enabled := False;
    edtCaminhoImpressora.Color := clBtnFace;
   end;
 end;

end.
